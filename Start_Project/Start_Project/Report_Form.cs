﻿using Microsoft.Reporting.WinForms;
using Start_Project.ProjectADataSetTableAdapters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Start_Project
{
    public partial class Report_Form : Form
    {
        private ProjectADataSetTableAdapters.AdvisorTableAdapter tableAdapter;

        public Report_Form()
        {
            InitializeComponent();
        }

        private void Report_Form_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'projectADataSet.Evaluation' table. You can move, or remove it, as needed.
            this.evaluationTableAdapter.Fill(this.projectADataSet.Evaluation);
            // TODO: This line of code loads data into the 'projectADataSet.ProjectAdvisor' table. You can move, or remove it, as needed.
            this.projectAdvisorTableAdapter.Fill(this.projectADataSet.ProjectAdvisor);
            // TODO: This line of code loads data into the 'projectADataSet.GroupEvaluation' table. You can move, or remove it, as needed.
            this.groupEvaluationTableAdapter.Fill(this.projectADataSet.GroupEvaluation);
            // TODO: This line of code loads data into the 'projectADataSet.Group' table. You can move, or remove it, as needed.
            this.groupTableAdapter.Fill(this.projectADataSet.Group);
            // TODO: This line of code loads data into the 'projectADataSet.Person' table. You can move, or remove it, as needed.
            this.personTableAdapter.Fill(this.projectADataSet.Person);
            // TODO: This line of code loads data into the 'projectADataSet.Project' table. You can move, or remove it, as needed.
            this.projectTableAdapter.Fill(this.projectADataSet.Project);
            // TODO: This line of code loads data into the 'projectADataSetrpt1.Advisor' table. You can move, or remove it, as needed.
            this.advisorTableAdapter1.Fill(this.projectADataSetrpt1.Advisor);
            // TODO: This line of code loads data into the 'projectADataSet.Advisor' table. You can move, or remove it, as needed.
            this.advisorTableAdapter.Fill(this.projectADataSet.Advisor);

            this.reportViewer1.RefreshReport();
            this.reportViewer2.RefreshReport();
            this.reportViewer3.RefreshReport();
            this.reportViewer4.RefreshReport();
            this.reportViewer5.RefreshReport();
            this.reportViewer6.RefreshReport();
        }
        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }

        private void reportViewer2_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    } }
        /*

            // Create a new DataTable to hold the data
            YourDataSet.YourTableDataTable dataTable = new YourDataSet.YourTableDataTable();

            // Fill the DataTable with data from the database
            tableAdapter.Fill(dataTable);

            // Set the data source for the ReportViewer control
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("YourDataSet", dataTable));

            // Set the report path
            reportViewer1.LocalReport.ReportPath = "YourReport.rdlc";

            // Refresh the report
            reportViewer1.RefreshReport();*\
       
    }
}
        */