﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Start_Project.Forms
{
    public partial class manage_Advisor : Form
    {
        public manage_Advisor()
        {
            InitializeComponent();
        }
        private void manage_Advisor_Load(object sender, EventArgs e)
        {
            getfromDataBase();
            clearData();
        }
        private void addBTN_Click(object sender, EventArgs e)
        {
            int ID=0;
            string userInput = "";
            string desig= null;
            string salary = null;
            int salaryI = 0;
            int desigI = 0;
            try
            {
                userInput = IDtxt.Text; 
                desig = DesigBox.Text;
                salary = salarytxt.Text;
                if (string.IsNullOrWhiteSpace(userInput))
                {
                    MessageBox.Show("Avoid spaces and NULL in ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else 
                {
                    try 
                    {
                        ID=int.Parse(userInput);
                    }
                    catch
                    {
                        MessageBox.Show("ID can't contain Letter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                if (desig=="None")
                {
                       MessageBox.Show("Designation can't be None", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                }

                if(salary==""|| salary==null) { salary = null; }
                try
                {
                    salaryI = int.Parse(salary);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Salary can't contain Strings", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine("4");
                    return;
                }
                if (salaryI < 1)
                {
                    MessageBox.Show("Salary can't be Negative", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine("10");
                    return;
                }
                if (checkifPresent(ID))
                { 
                    MessageBox.Show("ID already Exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine("10");
                    return;
                }
                try
                { 
                    if (desig == "Professor")
                    {
                        desigI = 6;
                    }else if(desig == "Associate Professor")
                    {
                        desigI = 7;
                    }
                    else if (desig == "Assistant Professor")
                    {
                    desigI= 8;
                    }
                    else if (desig == "Lecturer")
                    {
                        desigI = 9;
                    }
                    else if (desig == "Industry Professional")
                    {
                        desigI=10;
                    }
                    try
                    {
                        var myConnect = Configuration.getInstance().getConnection();
                        string Query = "INSERT INTO Advisor (Id,Designation,Salary) Values (@ID,@designation,@Salary)";
                        using (SqlCommand connect = new SqlCommand(Query, myConnect))
                        {
                            connect.Parameters.AddWithValue("@ID", ID);
                            connect.Parameters.AddWithValue("@designation",desigI);
                                connect.Parameters.AddWithValue("@Salary",salaryI);
                                connect.ExecuteNonQuery();
                        }
                            getfromDataBase();
                            MessageBox.Show("Inserted");
                        clearData();
                    }
                    catch (Exception ex)
                    {
                            Console.WriteLine(ex.Message);
                    }

                }
                catch (Exception ex)
                {
                        Console.WriteLine(ex.Message);
                }

            }
            catch (Exception ex)
            {
                    Console.WriteLine(ex.Message);
            }            
        }
        public void getfromDataBase()
        {
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                string query = "SELECT * FROM Advisor";
                SqlCommand myCommand = new SqlCommand(query, myCon);
                SqlDataAdapter myAdapter = new SqlDataAdapter(myCommand);
                DataTable dt = new DataTable();
                myAdapter.Fill(dt);
                datagrvw.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private void dltBTN_Click(object sender, EventArgs e)
        {
            int ID = 0;
            string userInput = "";
            string desig = null;
            string salary = null;
            int salaryI = 0;
            int desigI = 0;
            try
            {
                userInput = IDtxt.Text;
                desig = DesigBox.Text;
                salary = salarytxt.Text;
                if (string.IsNullOrWhiteSpace(userInput))
                {
                    MessageBox.Show("Avoid spaces and NULL in ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    try
                    {
                        ID = int.Parse(userInput);
                    }
                    catch
                    {
                        MessageBox.Show("ID can't contain Letter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                if (desig == "None")
                {
                    MessageBox.Show("Designation can't be None", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (salary == "" || salary == null) { salary = null; }
                try
                {
                    salaryI = int.Parse(salary);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Salary can't contain Strings", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine("4");
                    return;
                }
                try
                {
                    if (desig == "Professor")
                    {
                        desigI = 6;
                    }
                    else if (desig == "Associate Professor")
                    {
                        desigI = 7;
                    }
                    else if (desig == "Assistant Professor")
                    {
                        desigI = 8;
                    }
                    else if (desig == "Lecturer")
                    {
                        desigI = 9;
                    }
                    else if (desig == "Industry Professional")
                    {
                        desigI = 10;
                    }
                    if(!checkifPresent(ID))
                    {
                        MessageBox.Show("Not Found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    try
                    {
                        var myConnect = Configuration.getInstance().getConnection();
                        string query = "Delete from Advisor WHERE Id=@ID AND Designation=@desig";
                        using (SqlCommand connect = new SqlCommand(query, myConnect))
                        {
                            connect.Parameters.AddWithValue("@ID", ID);
                            connect.Parameters.AddWithValue("@desig",desigI);
                            connect.ExecuteNonQuery();
                        }
                        getfromDataBase();
                        MessageBox.Show("Deleted");
                        clearData();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private bool checkifPresent(int ID)
        {
            SqlConnection connect = Configuration.getInstance().getConnection();
            string query = "SELECT * FROM Advisor WHERE Id = @ID";
            using (SqlCommand command = new SqlCommand(query, connect))
            {
                command.Parameters.AddWithValue("@ID", ID);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    try
                    {
                        if (reader.Read())
                        {
                            return true;
                        }
                    }
                    catch (Exception ex) { Console.WriteLine(ex.Message); }
                }

            }
            return false;
        }
        private void clrBTN_Click(object sender, EventArgs e)
        {
            clearData();
        }
        private void clearData()
        {
            IDtxt.Text = "";
            salarytxt.Text = "";
            DesigBox.Text = "None";
        }
        private void BTNupdate_Click(object sender, EventArgs e)
        {
            int ID = 0;
            string userInput = "";
            string desig = null;
            string salary = null;
            int salaryI = 0;
            int desigI = 0;
            try
            {
                userInput = IDtxt.Text;
                desig = DesigBox.Text;
                salary = salarytxt.Text;
                if (string.IsNullOrWhiteSpace(userInput))
                {
                    MessageBox.Show("Avoid spaces and NULL in ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    try
                    {
                        ID = int.Parse(userInput);
                    }
                    catch
                    {
                        MessageBox.Show("ID can't contain Letter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                if (desig == "None")
                {
                    MessageBox.Show("Designation can't be None", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (salary == "" || salary == null) { salary = null; }
                try
                {
                    salaryI = int.Parse(salary);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Salary can't contain Strings", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (salaryI < 1)
                {
                    MessageBox.Show("Salary can't be Negative", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine("10");
                    return;
                }
                try
                {
                    if (desig == "Professor")
                    {
                        desigI = 6;
                    }
                    else if (desig == "Associate Professor")
                    {
                        desigI = 7;
                    }
                    else if (desig == "Assistant Professor")
                    {
                        desigI = 8;
                    }
                    else if (desig == "Lecturer")
                    {
                        desigI = 9;
                    }
                    else if (desig == "Industry Professional")
                    {
                        desigI = 10;
                    }
                    if (!checkifPresent(ID))
                    {

                        MessageBox.Show("Not Found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                        try
                        {
                            var myConnect = Configuration.getInstance().getConnection();
                            string query = "UPDATE Advisor SET Designation=@designation,Salary=@Salary WHERE Id=@ID";
                            using (SqlCommand connect = new SqlCommand(query, myConnect))
                            {
                                connect.Parameters.AddWithValue("@ID", ID);
                                connect.Parameters.AddWithValue("@designation", desigI);
                                connect.Parameters.AddWithValue("@Salary", salaryI);
                                connect.ExecuteNonQuery();
                            }
                            getfromDataBase();
                            MessageBox.Show("Updated");
                            clearData();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void datagrvw_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int id;
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = datagrvw.Rows[e.RowIndex];
                    id = Convert.ToInt32(row.Cells["Id"].Value);
                    IDtxt.Text = id.ToString();
                    salarytxt.Text = (row.Cells["Salary"].Value).ToString();
                    int num = int.Parse(row.Cells["Designation"].Value.ToString());
                    string desig = "";
                    if (num == 6)
                    {
                        desig = "Professor"; 
                    }
                    else if(num == 7)
                    { 
                        desig = "Associate Professor"; 
                    }
                    
                    else if(num == 8)
                    {
                            desig = "Assistant Professor";
                    }
                    else if(num == 9)
                    {
                        desig = "Lecturer"; 
                    }
                    else if(num == 10)
                    { 
                        desig = "Industry Professional";
                    };
                    DesigBox.Text=desig;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void bckBTN_Click(object sender, EventArgs e)
        {
            Program.FirstForm.Show();
            this.Close();
        }
        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }
        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }
        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
        private void salarytxt_TextChanged(object sender, EventArgs e)
        {

        }
        private void label2_Click(object sender, EventArgs e)
        {

        }
        private void IDLBL_Click(object sender, EventArgs e)
        {

        }
        private void IDtxt_TextChanged(object sender, EventArgs e)
        {

        }
        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void DesigBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
