﻿namespace Start_Project.Forms
{
    partial class Manage_Students
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.GenderB = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.IDLBL = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.IDtxt = new System.Windows.Forms.TextBox();
            this.DOBtxt = new System.Windows.Forms.DateTimePicker();
            this.EMAILtxt = new System.Windows.Forms.TextBox();
            this.LNAMEtxt = new System.Windows.Forms.TextBox();
            this.FNAMEtxt = new System.Windows.Forms.TextBox();
            this.CONTACTtxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridViewStudent = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.idstdnt = new System.Windows.Forms.TextBox();
            this.regnotxt = new System.Windows.Forms.TextBox();
            this.dltStdnt = new System.Windows.Forms.Button();
            this.AddStudentBTN = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.showSTDNT = new System.Windows.Forms.Button();
            this.datagrvw = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.clrBTN = new System.Windows.Forms.Button();
            this.bckBTN = new System.Windows.Forms.Button();
            this.BTNupdate = new System.Windows.Forms.Button();
            this.dltBTN = new System.Windows.Forms.Button();
            this.addBTN = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudent)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagrvw)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.008584F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.4721F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.50215F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.50215F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.87554F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.65665F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.67382F));
            this.tableLayoutPanel1.Controls.Add(this.GenderB, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.label6, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.IDLBL, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.IDtxt, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.DOBtxt, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.EMAILtxt, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.LNAMEtxt, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.FNAMEtxt, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.CONTACTtxt, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 3, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1165, 120);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // GenderB
            // 
            this.GenderB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GenderB.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GenderB.FormattingEnabled = true;
            this.GenderB.Items.AddRange(new object[] {
            "Male",
            "Female",
            "None"});
            this.GenderB.Location = new System.Drawing.Point(1003, 57);
            this.GenderB.MinimumSize = new System.Drawing.Size(103, 0);
            this.GenderB.Name = "GenderB";
            this.GenderB.Size = new System.Drawing.Size(159, 54);
            this.GenderB.TabIndex = 1;
            this.GenderB.Click += new System.EventHandler(this.GenderB_Click);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1003, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(159, 54);
            this.label6.TabIndex = 7;
            this.label6.Text = "Gender";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(231, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 54);
            this.label2.TabIndex = 3;
            this.label2.Text = "Last Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(86, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 54);
            this.label1.TabIndex = 2;
            this.label1.Text = "First Name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // IDLBL
            // 
            this.IDLBL.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IDLBL.AutoSize = true;
            this.IDLBL.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.IDLBL.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDLBL.Location = new System.Drawing.Point(3, 0);
            this.IDLBL.Name = "IDLBL";
            this.IDLBL.Size = new System.Drawing.Size(77, 54);
            this.IDLBL.TabIndex = 1;
            this.IDLBL.Text = "ID";
            this.IDLBL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(730, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(267, 54);
            this.label5.TabIndex = 6;
            this.label5.Text = "Date of Birth";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // IDtxt
            // 
            this.IDtxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IDtxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDtxt.Location = new System.Drawing.Point(3, 57);
            this.IDtxt.Multiline = true;
            this.IDtxt.Name = "IDtxt";
            this.IDtxt.Size = new System.Drawing.Size(77, 60);
            this.IDtxt.TabIndex = 0;
            this.IDtxt.Click += new System.EventHandler(this.IDtxt_Click);
            // 
            // DOBtxt
            // 
            this.DOBtxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DOBtxt.Location = new System.Drawing.Point(730, 57);
            this.DOBtxt.MaxDate = new System.DateTime(2024, 12, 31, 0, 0, 0, 0);
            this.DOBtxt.MinimumSize = new System.Drawing.Size(10, 57);
            this.DOBtxt.Name = "DOBtxt";
            this.DOBtxt.Size = new System.Drawing.Size(267, 57);
            this.DOBtxt.TabIndex = 13;
            this.DOBtxt.Value = new System.DateTime(2024, 5, 2, 0, 0, 0, 0);
            this.DOBtxt.ValueChanged += new System.EventHandler(this.DOBtxt_ValueChanged);
            // 
            // EMAILtxt
            // 
            this.EMAILtxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EMAILtxt.Location = new System.Drawing.Point(551, 57);
            this.EMAILtxt.Multiline = true;
            this.EMAILtxt.Name = "EMAILtxt";
            this.EMAILtxt.Size = new System.Drawing.Size(173, 60);
            this.EMAILtxt.TabIndex = 11;
            this.EMAILtxt.Click += new System.EventHandler(this.EMAILtxt_Click);
            // 
            // LNAMEtxt
            // 
            this.LNAMEtxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LNAMEtxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LNAMEtxt.Location = new System.Drawing.Point(231, 57);
            this.LNAMEtxt.Multiline = true;
            this.LNAMEtxt.Name = "LNAMEtxt";
            this.LNAMEtxt.Size = new System.Drawing.Size(154, 60);
            this.LNAMEtxt.TabIndex = 9;
            this.LNAMEtxt.Click += new System.EventHandler(this.LNAMEtxt_Click);
            // 
            // FNAMEtxt
            // 
            this.FNAMEtxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FNAMEtxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FNAMEtxt.Location = new System.Drawing.Point(86, 57);
            this.FNAMEtxt.Multiline = true;
            this.FNAMEtxt.Name = "FNAMEtxt";
            this.FNAMEtxt.Size = new System.Drawing.Size(139, 60);
            this.FNAMEtxt.TabIndex = 8;
            this.FNAMEtxt.Click += new System.EventHandler(this.FNAMEtxt_Click);
            // 
            // CONTACTtxt
            // 
            this.CONTACTtxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CONTACTtxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CONTACTtxt.Location = new System.Drawing.Point(391, 57);
            this.CONTACTtxt.Multiline = true;
            this.CONTACTtxt.Name = "CONTACTtxt";
            this.CONTACTtxt.Size = new System.Drawing.Size(154, 60);
            this.CONTACTtxt.TabIndex = 10;
            this.CONTACTtxt.Click += new System.EventHandler(this.CONTACTtxt_Click);
            this.CONTACTtxt.TextChanged += new System.EventHandler(this.CONTACTtxt_TextChanged);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(551, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(173, 54);
            this.label4.TabIndex = 5;
            this.label4.Text = "Email";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(391, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 54);
            this.label3.TabIndex = 4;
            this.label3.Text = "Contact";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.42857F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 343F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.57143F));
            this.tableLayoutPanel2.Controls.Add(this.dataGridViewStudent, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel5, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.datagrvw, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 211);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 504F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1162, 504);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // dataGridViewStudent
            // 
            this.dataGridViewStudent.AllowUserToAddRows = false;
            this.dataGridViewStudent.AllowUserToDeleteRows = false;
            this.dataGridViewStudent.AllowUserToResizeColumns = false;
            this.dataGridViewStudent.AllowUserToResizeRows = false;
            this.dataGridViewStudent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewStudent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStudent.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewStudent.Location = new System.Drawing.Point(588, 3);
            this.dataGridViewStudent.MultiSelect = false;
            this.dataGridViewStudent.Name = "dataGridViewStudent";
            this.dataGridViewStudent.ReadOnly = true;
            this.dataGridViewStudent.RowHeadersWidth = 51;
            this.dataGridViewStudent.RowTemplate.Height = 24;
            this.dataGridViewStudent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewStudent.Size = new System.Drawing.Size(337, 498);
            this.dataGridViewStudent.TabIndex = 4;
            this.dataGridViewStudent.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewStudent_CellContentClick);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.idstdnt, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.regnotxt, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.dltStdnt, 0, 5);
            this.tableLayoutPanel5.Controls.Add(this.AddStudentBTN, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this.label8, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.showSTDNT, 0, 6);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(931, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 7;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.14393F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.55134F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.232932F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.05622F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.46586F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.26104F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.08032F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(228, 498);
            this.tableLayoutPanel5.TabIndex = 3;
            this.tableLayoutPanel5.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel5_Paint);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(222, 50);
            this.label7.TabIndex = 21;
            this.label7.Text = "Registration No";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // idstdnt
            // 
            this.idstdnt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.idstdnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idstdnt.Location = new System.Drawing.Point(3, 171);
            this.idstdnt.Multiline = true;
            this.idstdnt.Name = "idstdnt";
            this.idstdnt.Size = new System.Drawing.Size(222, 51);
            this.idstdnt.TabIndex = 17;
            this.idstdnt.TextChanged += new System.EventHandler(this.idstdnt_TextChanged);
            // 
            // regnotxt
            // 
            this.regnotxt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.regnotxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.regnotxt.Location = new System.Drawing.Point(3, 53);
            this.regnotxt.Multiline = true;
            this.regnotxt.Name = "regnotxt";
            this.regnotxt.Size = new System.Drawing.Size(222, 46);
            this.regnotxt.TabIndex = 16;
            this.regnotxt.TextChanged += new System.EventHandler(this.regnotxt_TextChanged_1);
            // 
            // dltStdnt
            // 
            this.dltStdnt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dltStdnt.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.dltStdnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dltStdnt.Location = new System.Drawing.Point(3, 323);
            this.dltStdnt.Name = "dltStdnt";
            this.dltStdnt.Size = new System.Drawing.Size(222, 58);
            this.dltStdnt.TabIndex = 19;
            this.dltStdnt.Text = "DELETE Student";
            this.dltStdnt.UseVisualStyleBackColor = false;
            this.dltStdnt.Click += new System.EventHandler(this.dltStdnt_Click);
            // 
            // AddStudentBTN
            // 
            this.AddStudentBTN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AddStudentBTN.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.AddStudentBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddStudentBTN.Location = new System.Drawing.Point(3, 241);
            this.AddStudentBTN.Name = "AddStudentBTN";
            this.AddStudentBTN.Size = new System.Drawing.Size(222, 60);
            this.AddStudentBTN.TabIndex = 20;
            this.AddStudentBTN.Text = "Add Student";
            this.AddStudentBTN.UseVisualStyleBackColor = false;
            this.AddStudentBTN.Click += new System.EventHandler(this.AddStudentBTN_Click);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 127);
            this.label8.MinimumSize = new System.Drawing.Size(222, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(222, 40);
            this.label8.TabIndex = 22;
            this.label8.Text = "ID";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // showSTDNT
            // 
            this.showSTDNT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.showSTDNT.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.showSTDNT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.showSTDNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showSTDNT.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.showSTDNT.Location = new System.Drawing.Point(3, 416);
            this.showSTDNT.MinimumSize = new System.Drawing.Size(100, 31);
            this.showSTDNT.Name = "showSTDNT";
            this.showSTDNT.Size = new System.Drawing.Size(222, 62);
            this.showSTDNT.TabIndex = 18;
            this.showSTDNT.Text = "Show Student";
            this.showSTDNT.UseVisualStyleBackColor = false;
            this.showSTDNT.Click += new System.EventHandler(this.showSTDNT_Click_1);
            // 
            // datagrvw
            // 
            this.datagrvw.AllowUserToAddRows = false;
            this.datagrvw.AllowUserToDeleteRows = false;
            this.datagrvw.AllowUserToResizeColumns = false;
            this.datagrvw.AllowUserToResizeRows = false;
            this.datagrvw.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.datagrvw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagrvw.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.datagrvw.Location = new System.Drawing.Point(3, 3);
            this.datagrvw.MultiSelect = false;
            this.datagrvw.Name = "datagrvw";
            this.datagrvw.ReadOnly = true;
            this.datagrvw.RowHeadersWidth = 51;
            this.datagrvw.RowTemplate.Height = 24;
            this.datagrvw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.datagrvw.Size = new System.Drawing.Size(579, 498);
            this.datagrvw.TabIndex = 0;
            this.datagrvw.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.datagrvw_CellContentClick_1);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.11896F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.25078F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.39423F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.37832F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.BTNupdate, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.dltBTN, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.addBTN, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(139, 145);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(870, 60);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.87218F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.12782F));
            this.tableLayoutPanel4.Controls.Add(this.clrBTN, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.bckBTN, 0, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(399, 54);
            this.tableLayoutPanel4.TabIndex = 7;
            // 
            // clrBTN
            // 
            this.clrBTN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.clrBTN.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.clrBTN.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.clrBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clrBTN.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.clrBTN.Location = new System.Drawing.Point(198, 3);
            this.clrBTN.MinimumSize = new System.Drawing.Size(100, 31);
            this.clrBTN.Name = "clrBTN";
            this.clrBTN.Size = new System.Drawing.Size(198, 48);
            this.clrBTN.TabIndex = 6;
            this.clrBTN.Text = "Clear";
            this.clrBTN.UseVisualStyleBackColor = false;
            this.clrBTN.Click += new System.EventHandler(this.clrBTN_Click);
            // 
            // bckBTN
            // 
            this.bckBTN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bckBTN.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bckBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bckBTN.Location = new System.Drawing.Point(3, 3);
            this.bckBTN.Name = "bckBTN";
            this.bckBTN.Size = new System.Drawing.Size(189, 48);
            this.bckBTN.TabIndex = 3;
            this.bckBTN.Text = "Back";
            this.bckBTN.UseVisualStyleBackColor = true;
            this.bckBTN.Click += new System.EventHandler(this.bckBTN_Click);
            // 
            // BTNupdate
            // 
            this.BTNupdate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BTNupdate.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.BTNupdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTNupdate.Location = new System.Drawing.Point(706, 3);
            this.BTNupdate.Name = "BTNupdate";
            this.BTNupdate.Size = new System.Drawing.Size(161, 54);
            this.BTNupdate.TabIndex = 2;
            this.BTNupdate.Text = "UPDATE";
            this.BTNupdate.UseVisualStyleBackColor = false;
            this.BTNupdate.Click += new System.EventHandler(this.BTNupdate_Click);
            // 
            // dltBTN
            // 
            this.dltBTN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dltBTN.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.dltBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dltBTN.Location = new System.Drawing.Point(531, 3);
            this.dltBTN.Name = "dltBTN";
            this.dltBTN.Size = new System.Drawing.Size(169, 54);
            this.dltBTN.TabIndex = 4;
            this.dltBTN.Text = "DELETE";
            this.dltBTN.UseVisualStyleBackColor = false;
            this.dltBTN.Click += new System.EventHandler(this.dltBTN_Click);
            // 
            // addBTN
            // 
            this.addBTN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.addBTN.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.addBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addBTN.Location = new System.Drawing.Point(408, 3);
            this.addBTN.Name = "addBTN";
            this.addBTN.Size = new System.Drawing.Size(117, 54);
            this.addBTN.TabIndex = 0;
            this.addBTN.Text = "ADD";
            this.addBTN.UseVisualStyleBackColor = false;
            this.addBTN.Click += new System.EventHandler(this.addBTN_Click);
            // 
            // Manage_Students
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1164, 727);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximumSize = new System.Drawing.Size(1437, 802);
            this.MinimumSize = new System.Drawing.Size(1182, 774);
            this.Name = "Manage_Students";
            this.Text = "Manage_Students";
            this.Load += new System.EventHandler(this.Manage_Students_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudent)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagrvw)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox IDtxt;
        private System.Windows.Forms.Label IDLBL;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox EMAILtxt;
        private System.Windows.Forms.TextBox CONTACTtxt;
        private System.Windows.Forms.TextBox LNAMEtxt;
        private System.Windows.Forms.TextBox FNAMEtxt;
        private System.Windows.Forms.ComboBox GenderB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button addBTN;
        private System.Windows.Forms.Button dltBTN;
        private System.Windows.Forms.Button BTNupdate;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.DataGridView datagrvw;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button clrBTN;
        private System.Windows.Forms.Button bckBTN;
        private System.Windows.Forms.DateTimePicker DOBtxt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TextBox regnotxt;
        private System.Windows.Forms.Button showSTDNT;
        private System.Windows.Forms.TextBox idstdnt;
        private System.Windows.Forms.Button dltStdnt;
        private System.Windows.Forms.Button AddStudentBTN;
        private System.Windows.Forms.DataGridView dataGridViewStudent;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}