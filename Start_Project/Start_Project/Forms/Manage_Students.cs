﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Start_Project;
using System.Configuration;
using Start_Project.BL;
using Start_Project.DL;
using static System.Collections.Specialized.BitVector32;
using System.Net;
using System.Text.RegularExpressions;
using EZInput;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;
using System.Runtime.ExceptionServices;

namespace Start_Project.Forms
{
    public partial class Manage_Students : Form
    {
        bool clr = true;
        int rwIndex = 0;
        public Manage_Students()
        {
            InitializeComponent();
            clearData();
        }
        private void Manage_Students_Load(object sender, EventArgs e)
        {
            getfromDataBase();
            clearStudent();
        }
        private void clearData()
        {
            try
            {
                GenderB.Text = "None";
                IDtxt.Text = "Auto";
                IDtxt.ReadOnly = true;
                EMAILtxt.Text = "abc@mail.com";
                CONTACTtxt.Text = "";
                LNAMEtxt.Text = "";
                FNAMEtxt.Text = "";
                DOBtxt.Value = DateTime.Today;
            }
            catch (Exception ex) { }

        }
        public void getfromDataBase()
        {
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                string query = "SELECT * FROM Person Where Gender!=" + 4 + "";
                SqlCommand myCommand = new SqlCommand(query, myCon);
                SqlDataAdapter myAdapter = new SqlDataAdapter(myCommand);
                DataTable dt = new DataTable();
                myAdapter.Fill(dt);
                datagrvw.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private void ShowData()
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void addBTN_Click(object sender, EventArgs e)
        {
            string fname;
            string lname;
            string contact;
            string email;
            DateTime? DOB = null;
            int gend;
            String datetime=null;
            try
            {
                //int ID = int.Parse(IDtxt.Text.ToString());
                fname = FNAMEtxt.Text;
                if (fname == null || fname.Length == 0 || fname.Length > 100)
                {
                    MessageBox.Show("An error occurred : First Name can't be NULL and Smaller than 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                lname = LNAMEtxt.Text;
                if (lname.Length > 100)
                {
                    MessageBox.Show("An error occurred : Last Name must be Smaller than 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (lname == "")
                {
                    lname = null;
                }
                contact = CONTACTtxt.Text;
                if (contact == "" || contact == null)
                {
                    contact = null;
                }
                else if (contact.Length > 20)
                {
                    MessageBox.Show("An error occurred : Contact must be Smaller than 20", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                email = EMAILtxt.Text;
                if (email == "")
                {
                    MessageBox.Show("An error occurred : Your Email can't be NULL", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                int maxEmail = 30;
                if (email.Length > maxEmail)
                {
                    MessageBox.Show("An error occurred : Your Email Length should be Smaller than 30", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string pattern = @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$";
                bool isValidEmail = Regex.IsMatch(email, pattern);
                if (!isValidEmail)
                {
                    MessageBox.Show("An error occurred : Your Email Format is Incorrext", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (DOBtxt.Value == DateTime.Today)
                {
                    datetime = null;
                }
                else if (DOBtxt.Value > DateTime.Today)
                {
                    MessageBox.Show("Please enter a date of birth  before Today's Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if(DOBtxt.Value < DateTime.Today) 
                {
                    DOB = DOBtxt.Value;
                }
                string gender = GenderB.Text.ToString();
                if (gender == "Male")
                {
                    gend = 1;
                }
                else if (gender == "Female") 
                {
                    gend = 2; 
                }
                else 
                { gend = 0; }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                string firstName = fname;
                string lastName = lname;
                string Contact = contact;
                string emailID = email;
                string query = "INSERT INTO Person Values (@FirstName,@LastName,@Contact,@Email,@DateOfBirth,@Gender)";
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@FirstName", firstName);
                    if (lastName == null)
                    {
                        myCommand.Parameters.AddWithValue("@LastName", DBNull.Value);
                    }
                    else
                    {
                        myCommand.Parameters.AddWithValue("@LastName", lastName);
                    }
                    if (contact == null)
                    {
                        myCommand.Parameters.AddWithValue("@Contact", DBNull.Value);
                    }
                    else
                    {
                        myCommand.Parameters.AddWithValue("@Contact", Contact);
                    }
                    myCommand.Parameters.AddWithValue("@Email", emailID);
                    if (datetime == null)
                    {
                        myCommand.Parameters.AddWithValue("@DateOfBirth", DBNull.Value);
                    }
                    else
                    {
                        myCommand.Parameters.AddWithValue("@DateOfBirth", DOB);
                    }
                    if (gend == 0)
                    {
                        myCommand.Parameters.AddWithValue("@Gender", DBNull.Value);
                    }
                    else
                    {
                        myCommand.Parameters.AddWithValue("@Gender", gend);
                    }
                    myCommand.ExecuteNonQuery();
                }
                clearData();
                getfromDataBase();
                MessageBox.Show("Inserted");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void CONTACTtxt_TextChanged(object sender, EventArgs e)
        { }
        private void dltBTN_Click(object sender, EventArgs e)
        {
           
                int ID=0;
                try { ID = int.Parse(IDtxt.Text.ToString()); }catch(Exception ex) { return; }
                DialogResult result = MessageBox.Show("It will Delete: " + ID + "\n Do you want to Delete?", "Delete:", MessageBoxButtons.YesNo, MessageBoxIcon.Question);                
                if(result==DialogResult.Yes)
                {
                    
                    try
                    {
                        var myCon = Configuration.getInstance().getConnection();
                        string query = "UPDATE Person SET Gender=@Status WHERE Id=@ID";
                        using (SqlCommand myCommand = new SqlCommand(query, myCon))
                        {
                            myCommand.Parameters.AddWithValue("@ID", ID);
                            myCommand.Parameters.AddWithValue("@Status",4);
                            myCommand.ExecuteNonQuery();
                        }
                        MessageBox.Show("DELETED");
                        getfromDataBase();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                else
                { 
                    clrBTN_Click(sender, e);
                    return;
                }
           
        }
        private void datagrvw_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int id;
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = datagrvw.Rows[e.RowIndex];
                    id = Convert.ToInt32(row.Cells["Id"].Value);
                    IDtxt.Text = id.ToString();
                    EMAILtxt.Text = (row.Cells["Email"].Value).ToString();
                    CONTACTtxt.Text = (row.Cells["Contact"].Value).ToString();
                    LNAMEtxt.Text = (row.Cells["LastName"].Value).ToString();
                    FNAMEtxt.Text = (row.Cells["FirstName"].Value).ToString();
                    string DOB = (row.Cells["DateOfBirth"].Value).ToString();
                    string gender = (row.Cells["Gender"].Value).ToString();
                    if(int.Parse(gender) == 1)
                    {
                        GenderB.Text = "Male";
                    }
                    else if(int.Parse(gender) == 2)
                    {
                        GenderB.Text = "Female";
                    }
                    else
                    {
                        GenderB.Text = "None";
                    }
                    if (DOB == "")
                    {
                        DOBtxt.Text = "dd/mm/yyyy";
                    }
                    else
                    {
                        try
                        {
                            DateTime dt = Convert.ToDateTime(DOB);                           
                            DOBtxt.Value = dt;
                        }
                        catch (Exception ex)

                        {
                            Console.WriteLine("ex.Message");
                            Console.WriteLine(ex.Message); return;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("ex.Message 2");
                Console.WriteLine(ex.Message);
            }
        }
        private void EMAILtxt_Click(object sender, EventArgs e)
        {
            EMAILtxt.Text = "";
        }
        private void CONTACTtxt_Click(object sender, EventArgs e)
        {
            CONTACTtxt.Text = "";
        }
        private void LNAMEtxt_Click(object sender, EventArgs e)
        {
            LNAMEtxt.Text = "";
        }
        private void FNAMEtxt_Click(object sender, EventArgs e)
        {
            FNAMEtxt.Text = "";
        }
        private void IDtxt_Click(object sender, EventArgs e)
        {
            //IDtxt.Text = "";
        }
        private void GenderB_Click(object sender, EventArgs e)
        {
        }
        private void DOBtxt_Click(object sender, EventArgs e)
        {
            DOBtxt.Text = "";
        }
        private void clrBTN_Click(object sender, EventArgs e)
        {
            clr = true;
            clearData();
        }
        private void BTNupdate_Click(object sender, EventArgs e)
        {
                string fname;
                string lname;
                string contact;
                string email;
                DateTime? DOB = null;
                int gend;
                String datetime=null;
                int ID;
                try
                {
                    ID = int.Parse(IDtxt.Text.ToString());
                    fname = FNAMEtxt.Text;
                    if (fname == null || fname.Length == 0 || fname.Length > 100)
                    {
                        MessageBox.Show("An error occurred : First Name can't be NULL and Smaller than 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    lname = LNAMEtxt.Text;
                    if (lname.Length > 100)
                    {
                        MessageBox.Show("An error occurred : Last Name must be Smaller than 100", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (lname == "")
                    {
                        lname = null;
                    }
                    contact = CONTACTtxt.Text;
                    if (contact == "")
                    {
                        contact = null;
                    }
                    if (contact.Length > 20)
                    {
                        MessageBox.Show("An error occurred : Contact must be Smaller than 20", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    email = EMAILtxt.Text;
                    if (email == "")
                    {
                        MessageBox.Show("An error occurred : Your Email can't be NULL", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    int maxEmail = 30;
                    if (email.Length > maxEmail)
                    {
                        MessageBox.Show("An error occurred : Your Email Length should be Smaller than 30", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    string pattern = @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$";
                    bool isValidEmail = Regex.IsMatch(email, pattern);
                    if (!isValidEmail)
                    {
                        MessageBox.Show("An error occurred : Your Email Format is Incorrext", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    
                    if (DOBtxt.Value==DateTime.Today)
                    {
                        datetime = null;
                    }
                    else if (DOBtxt.Value > DateTime.Today)
                    {
                    MessageBox.Show("Please enter a date of birth  before Today's Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                    }
                    else if (DOBtxt.Value < DateTime.Today)
                    {
                    DOB = DOBtxt.Value;
                    }
                string gender = GenderB.Text.ToString();
                    if (gender == "Male")
                    {
                        gend = 1;
                    }
                    else if (gender == "Female") { gend = 2; }
                    else { gend = 0; }
                    //MessageBox.Show("PARSED");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return;
                }
                try
                {
                    var myCon = Configuration.getInstance().getConnection();
                    string firstName = fname;
                    string lastName = lname;
                    string Contact = contact;
                    string emailID = email;
                    string query = "UPDATE Person SET FirstName=@FirstName,LastName=@LastName,Contact=@Contact,Email=@Email,DateOfBirth=@DateOfBirth,Gender=@Gender WHERE Id=@ID";
                    using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    {
                        myCommand.Parameters.AddWithValue("@ID", ID);
                        myCommand.Parameters.AddWithValue("@FirstName", firstName);
                        if (lastName == null)
                        {
                            myCommand.Parameters.AddWithValue("@LastName", DBNull.Value);
                        }
                        else
                        {
                            myCommand.Parameters.AddWithValue("@LastName", lastName);
                        }
                        if (contact == null)
                        {
                            myCommand.Parameters.AddWithValue("@Contact", DBNull.Value);
                        }
                        else
                        {
                            myCommand.Parameters.AddWithValue("@Contact", Contact);
                        }
                        myCommand.Parameters.AddWithValue("@Email", emailID);
                        if (datetime == null)
                        {
                            myCommand.Parameters.AddWithValue("@DateOfBirth", DBNull.Value);
                        }
                        else
                        {
                            myCommand.Parameters.AddWithValue("@DateOfBirth", DOB);
                        }
                        if (gend == 0)
                        {
                            myCommand.Parameters.AddWithValue("@Gender", DBNull.Value);
                        }
                        else
                        {
                            myCommand.Parameters.AddWithValue("@Gender", gend);
                        }
                        myCommand.ExecuteNonQuery();
                    }
                    getfromDataBase();
                    MessageBox.Show("Updated");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
           
        }
        private bool checkIfPresent(int ID)
        {
            SqlConnection connect = Configuration.getInstance().getConnection();
            string query = "SELECT * FROM Person WHERE Id = @ID";
            using (SqlCommand command = new SqlCommand(query, connect))
            {
                command.Parameters.AddWithValue("@ID", ID);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    try
                    {
                        if (!reader.Read())
                        {
                            return false;
                        }
                    }
                    catch (Exception ex) { }
                }
            }
            return true;
        }
        private void bckBTN_Click(object sender, EventArgs e)
        {
           Program.FirstForm.Show();
           this.Close();
            
        }
        private void txtID_TextChanged(object sender, EventArgs e)
        {

        }
        private void showSTDNT_Click(object sender, EventArgs e)
        {
            showSTDNT1();
            clearStudent();
        }
        void showSTDNT1()
        {
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                string query = "SELECT * FROM Student";
                //s  INNER JOIN Person p ON s.Id=p.Id Where p.Gender!=" + 4 + "";
                SqlCommand myCommand = new SqlCommand(query, myCon);
                SqlDataAdapter myAdapter = new SqlDataAdapter(myCommand);
                DataTable dt = new DataTable();
                myAdapter.Fill(dt);
                dataGridViewStudent.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void DOBtxt_ValueChanged(object sender, EventArgs e)
        {

        }
        private void regnotxt_TextChanged(object sender, EventArgs e)
        {

        }

     //Student Function

        private void AddStudentBTN_Click(object sender, EventArgs e)
        {
            string RegNo = regnotxt.Text;
            string id = idstdnt.Text;
            int ID = 0;
            if (string.IsNullOrWhiteSpace(RegNo))
            {
                MessageBox.Show("Avoid spaces and NULL in Reg No", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if(RegNo.Length > 20)
            {
                MessageBox.Show(" Reg No too Long to Accept", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (string.IsNullOrWhiteSpace(id))
            {
                MessageBox.Show("Avoid spaces and NULL in ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                ID = int.Parse(id);

            }
            catch(Exception ex)
            {
                MessageBox.Show("ID can't contain Strings", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(regNoExist(RegNo))
            {
                MessageBox.Show("Registration Number should be Unique", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if(checkIfPresent(ID) && notStudent(ID))
            { 
                var Connection=Configuration.getInstance().getConnection();
                string query = "INSERT INTO Student (Id,RegistrationNo) Values (@ID,@RegNo)";
                SqlCommand newCommand=new SqlCommand(query, Connection);
                newCommand.Parameters.AddWithValue("@ID", ID);
                newCommand.Parameters.AddWithValue("@RegNo",RegNo);
                newCommand.ExecuteNonQuery();
                showSTDNT1();
                clearStudent();
                MessageBox.Show("Inserted in Student");
            }
            else
            {
                MessageBox.Show("ID Used Or No student", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


        }
        private bool notStudent(int ID)
        {
            SqlConnection connect = Configuration.getInstance().getConnection();
            string query = "SELECT * FROM Student WHERE Id = @ID";
            using (SqlCommand command = new SqlCommand(query, connect))
            {
                command.Parameters.AddWithValue("@ID", ID);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    try
                    {
                        if (reader.Read())
                        {
                            return false;
                        }
                    }
                    catch (Exception ex) { }
                }
            }
            return true;
        }
        private void clearStudent()
        {
            regnotxt.Text="";
            idstdnt.Text = "";

        }
        bool regNoExist(string RegNo)
        {
            SqlConnection connect = Configuration.getInstance().getConnection();
            string query = "SELECT * FROM Student WHERE RegistrationNo = @RegNo";
            using (SqlCommand command = new SqlCommand(query, connect))
            {
                command.Parameters.AddWithValue("@RegNo",RegNo);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    try
                    {
                        if (reader.Read())
                        {
                            return true;
                        }
                    }
                    catch (Exception ex) { }
                }
            }
            return false;
        }

        private void idstdnt_TextChanged(object sender, EventArgs e)
        {

        }
        private void dltStdnt_Click(object sender, EventArgs e)
        {
                string RegNo = regnotxt.Text;
            string id = idstdnt.Text;
            int ID = 0;
            if (string.IsNullOrWhiteSpace(RegNo))
            {
                    MessageBox.Show("Avoid spaces and NULL in Reg No", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            }
            else if (RegNo.Length > 20)
            {
                 MessageBox.Show(" Reg No too Long to Accept", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            }
            if (string.IsNullOrWhiteSpace(id))
            {
                    MessageBox.Show("Avoid spaces and NULL in ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            }
            try
                {
                    ID = int.Parse(id);

                }
            catch (Exception ex)
            {
                    MessageBox.Show("ID can't contain Strings", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            }
            if (checkIfPresent(ID))
                {
                try { 
                var Connection = Configuration.getInstance().getConnection();
                string query = "DELETE FROM Student WHERE Id=@ID AND RegistrationNo=@RegNo"; 
                SqlCommand newCommand = new SqlCommand(query, Connection);
                newCommand.Parameters.AddWithValue("@ID", ID);
                newCommand.Parameters.AddWithValue("@RegNo", RegNo);
                newCommand.ExecuteNonQuery();
                showSTDNT1();
                clearStudent();
                MessageBox.Show("Deleted From Student");
                }catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            else
            {
                    MessageBox.Show("ID does not Exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            }           
        }
        private void showSTDNT_Click_1(object sender, EventArgs e)
        {
            showSTDNT1();
        }
        private void tableLayoutPanel5_Paint(object sender, PaintEventArgs e)
        {

        }
        private void regnotxt_TextChanged_1(object sender, EventArgs e)
        {

        }
        private void dataGridViewStudent_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int id;
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = dataGridViewStudent.Rows[e.RowIndex];
                    id = Convert.ToInt32(row.Cells["Id"].Value);
                    idstdnt.Text = id.ToString();                    
                    string reg= row.Cells["RegistrationNo"].Value.ToString();
                    regnotxt.Text = reg;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
