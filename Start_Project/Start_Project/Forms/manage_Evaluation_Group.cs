﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Start_Project.Forms
{
    public partial class manage_Evaluation_Group : Form
    {
        List<int> EvaluationID = new List<int>();
        List<int> groupIDs = new List<int>();
        public manage_Evaluation_Group()
        {
            InitializeComponent();
        }
        private void manage_Evaluation_Group_Load(object sender, EventArgs e)
        {
            getfromDatabase1();
            getfromDatabase2();
            getfromDatabase();
            clearData();
        }
        private void addBTN_Click(object sender, EventArgs e)
        {
            string ObtMarks;
            int ObtMI = 0;
            DateTime? AssignDate = null;
            int groupI = 0, EvaluationI = 0;
            String datetime = null;
            int ID = 0;
            try
            {
                try
                {
                    groupI = int.Parse(grpIDbox.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred : Group ID can't contain String", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (groupI == 0)
                {
                    MessageBox.Show("An error occurred : Group ID can't be 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                EvaluationI = int.Parse(EvalIDBox.Text);
                if (EvaluationI == 0)
                {
                    MessageBox.Show("An error occurred : Evaluation can't be Null or 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                ObtMarks = obtMarkstxt.Text;
                if (string.IsNullOrEmpty(ObtMarks))
                {
                    MessageBox.Show("An error occurred :Obtained Marks can't be Null ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    try
                    {
                        ObtMI = int.Parse(ObtMarks);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("An error occurred : Marks can't be String ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (ObtMI < 0)
                    {
                        MessageBox.Show("An error occurred : Marks can't be Negative ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                if (Adatetxt.Value == DateTime.Today.AddDays(1))
                {
                    MessageBox.Show("Date can't be NULL", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (Adatetxt.Value > DateTime.Today)
                {
                    MessageBox.Show("Please enter a date in Past or Present.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    AssignDate = Adatetxt.Value;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("1" + ex.Message);
                return;
            }
            if (!checkIfPresent(groupI, "Group"))
            {
                MessageBox.Show("Group ID does not Exist", "Invalid GroupID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (checkifMemberComplete(groupI))
            {
                MessageBox.Show("Group's Evaluation Exceed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!checkIfPresent(EvaluationI, "Evaluation"))
            {
                MessageBox.Show("Evaluation ID does not Exist", "Invalid Evaluation", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (groupANDevaluationSame(groupI, EvaluationI))
            {
                MessageBox.Show("Evaluation ID Already Added", "Invalid Evaluation", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                string query = "INSERT INTO GroupEvaluation (GroupId,EvaluationId,ObtainedMarks,EvaluationDate) Values (@GroupId,@EvalID,@obtM,@EDate)";
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@GroupId", groupI);
                    myCommand.Parameters.AddWithValue("@EvalID", EvaluationI);
                    myCommand.Parameters.AddWithValue("@obtM", ObtMI);
                    myCommand.Parameters.AddWithValue("@EDate", AssignDate);
                    myCommand.ExecuteNonQuery();
                }
                clearData();
                getfromDatabase();
                MessageBox.Show("Inserted");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void BTNupdate_Click(object sender, EventArgs e)
        {
            string ObtMarks;
            int ObtMI = 0;
            DateTime? AssignDate = null;
            int groupI = 0, EvaluationI = 0;
            String datetime = null;
            int ID = 0;
            try
            {
                try
                {
                    groupI = int.Parse(grpIDbox.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred : Group ID can't contain String", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (groupI == 0)
                {
                    MessageBox.Show("An error occurred : Group ID can't be 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                EvaluationI = int.Parse(EvalIDBox.Text);
                if (EvaluationI == 0)
                {
                    MessageBox.Show("An error occurred : Evaluation can't be Null or 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                ObtMarks = obtMarkstxt.Text;
                if (string.IsNullOrEmpty(ObtMarks))
                {
                    MessageBox.Show("An error occurred :Obtained Marks can't be Null ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    try
                    {
                        ObtMI = int.Parse(ObtMarks);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("An error occurred : Marks can't be String ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (ObtMI < 0)
                    {
                        MessageBox.Show("An error occurred : Marks can't be Negative ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                if (Adatetxt.Value == DateTime.Today.AddDays(1))
                {
                    MessageBox.Show("Date can't be NULL", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (Adatetxt.Value > DateTime.Today)
                {
                    MessageBox.Show("Please enter a date in Past or Present.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    AssignDate = Adatetxt.Value;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("1" + ex.Message);
                return;
            }
            if (!checkIfPresent(groupI, "Group"))
            {
                MessageBox.Show("Group ID does not Exist", "Invalid GroupID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!checkIfPresent(EvaluationI, "Evaluation"))
            {
                MessageBox.Show("Evaluation ID does not Exist", "Invalid Evaluation", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                string query = "UPDATE GroupEvaluation SET ObtainedMarks=@obtM,EvaluationDate=@EDate WHERE GroupId=@GroupId AND EvaluationId=@EvalID";
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@GroupId", groupI);
                    myCommand.Parameters.AddWithValue("@EvalID", EvaluationI);
                    myCommand.Parameters.AddWithValue("@obtM",ObtMI);
                    myCommand.Parameters.AddWithValue("@EDate", AssignDate);
                    myCommand.ExecuteNonQuery();
                }
                clearData();
                getfromDatabase();
                MessageBox.Show("Updated");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void dltBTN_Click(object sender, EventArgs e)
        {
            string ObtMarks;
            int ObtMI = 0;
            DateTime? AssignDate = null;
            int groupI = 0, EvaluationI = 0;
            String datetime = null;
            int ID = 0;
            try
            {
                groupI = int.Parse(grpIDbox.Text);
                if (groupI == 0)
                {
                    MessageBox.Show("An error occurred : Group ID can't be Null or 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                EvaluationI = int.Parse(EvalIDBox.Text);
                if (EvaluationI == 0)
                {
                    MessageBox.Show("An error occurred : Eval can't be Null or 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                
            }
            catch (Exception ex)
            {

                Console.WriteLine("1" + ex.Message);
                return;
            }
            if (!checkIfPresent(groupI, "Group"))
            {
                MessageBox.Show("Group ID does not Exist", "Invalid GroupID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!checkIfPresent(EvaluationI, "Evaluation"))
            {
                MessageBox.Show("Evaluation ID does not Exist", "Invalid Evaluation", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                string query = "DELETE FROM GroupEvaluation WHERE GroupId=@GroupId AND EvaluationId=@EvalID";
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@GroupId", groupI);
                    myCommand.Parameters.AddWithValue("@EvalID", EvaluationI);
                    myCommand.ExecuteNonQuery();
                }
                clearData();
                getfromDatabase();
                MessageBox.Show("Deleted");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private bool groupANDevaluationSame(int groupI, int EvaluationI)
        {
            SqlConnection connect = Configuration.getInstance().getConnection();
            {
                string query = "SELECT * FROM GroupEvaluation WHERE GroupId = @ID AND EvaluationId=@EvID";
                using (SqlCommand command = new SqlCommand(query, connect))
                {
                    command.Parameters.AddWithValue("@ID", groupI);
                    command.Parameters.AddWithValue("@EvID", EvaluationI);
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
            }
        }
        private bool checkIfPresent(int ID, string tableName)
        {
            SqlConnection connect = Configuration.getInstance().getConnection();
            {
                string query = "SELECT * FROM [" + tableName + "] WHERE Id = @ID";
                using (SqlCommand command = new SqlCommand(query, connect))
                {
                    command.Parameters.AddWithValue("@ID", ID);
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
            }
        }
        private bool checkifMemberComplete(int ID)
        {

            SqlConnection connect = Configuration.getInstance().getConnection();
            {
                string query = "SELECT GroupId FROM GroupEvaluation WHERE GroupId = @ID GROUP BY GroupId HAVING COUNT(EvaluationId) > 5";
                using (SqlCommand command = new SqlCommand(query, connect))
                {
                    command.Parameters.AddWithValue("@ID", ID);
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
            }
        }
        private void clearData()
        {
            try
            {
                EvalIDBox.DataSource = EvaluationID;
                grpIDbox.DataSource = groupIDs;
                Adatetxt.Value = DateTime.Today.AddDays(1);
                EvalIDBox.Text = "";
                grpIDbox.Text = "";
                obtMarkstxt.Text = "";
            }
            catch (Exception ex) { }
        }
        private void getfromDatabase()
        {
            var myCon=Configuration.getInstance().getConnection();
            string query = "SELECT * FROM GroupEvaluation";
            SqlCommand myCommand=new SqlCommand(query,myCon);
            SqlDataAdapter myDataAdapter = new SqlDataAdapter(myCommand);
            DataTable dt=new DataTable();
            myDataAdapter.Fill(dt);
            datagrvw.DataSource= dt;           
        }
        private void getfromDatabase1()
        {
            var myCon = Configuration.getInstance().getConnection();
            string query = "SELECT Id FROM Evaluation";
            SqlCommand myCommand = new SqlCommand(query, myCon);
            SqlDataAdapter myDataAdapter = new SqlDataAdapter(myCommand);
            DataTable dt = new DataTable();
            myDataAdapter.Fill(dt);
            dataGridViewEvaluation.DataSource = dt;
            foreach (DataRow row in dt.Rows)
            {
                int id = Convert.ToInt32(row["Id"]);
                Console.WriteLine(id);
                EvaluationID.Add(id);
            }
            foreach (int id in EvaluationID)
            {
                Console.WriteLine(id);
            }
        }
        private void getfromDatabase2()
        {
            var myCon = Configuration.getInstance().getConnection();
            string query = "SELECT Id FROM [Group]";
            SqlCommand myCommand = new SqlCommand(query, myCon);
            SqlDataAdapter myDataAdapter = new SqlDataAdapter(myCommand);
            DataTable dt = new DataTable();
            myDataAdapter.Fill(dt);
            dataGridViewgrp.DataSource = dt;

            foreach (DataRow row in dt.Rows)
            {
                int id = Convert.ToInt32(row["Id"]);
                groupIDs.Add(id);
            }
        }
        private void datagrvw_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int grpid, evalid;
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = datagrvw.Rows[e.RowIndex];
                    grpid = Convert.ToInt32(row.Cells["GroupId"].Value);
                    grpIDbox.Text = grpid.ToString();
                    evalid = Convert.ToInt32(row.Cells["EvaluationId"].Value);
                    EvalIDBox.Text = evalid.ToString();
                    obtMarkstxt.Text = (row.Cells["ObtainedMarks"].Value).ToString();
                    Adatetxt.Value = Convert.ToDateTime(row.Cells["EvaluationDate"].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        private void clrBTN_Click(object sender, EventArgs e)
        {
            clearData();
        }
        private void bckBTN_Click(object sender, EventArgs e)
        {
            Program.FirstForm.Show();
            this.Close();
        }
    }
}
