﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace Start_Project.Forms
{
    public partial class manage_GroupStudent : Form
    {
        List<int> studentIDs = new List<int>();
        List<int> groupIDs = new List<int>();
        public manage_GroupStudent()
        {
            InitializeComponent();
            getfromDatabase2();
            getfromDatabase3();
            getfromDataBase();
            clearData();
        }
        private void studentIDBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        
        private void addBTN_Click(object sender, EventArgs e)
        {
            string status;
            int Status=0;
            DateTime? AssignDate = null;
            int groupI=0,studentI=0;
            String datetime = null;
            int ID = 0;
            try
            {
                groupI = int.Parse(grpIDbox.Text);
                if (groupI == 0)
                {
                    MessageBox.Show("An error occurred : Group ID can't be Null or 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                studentI = int.Parse(studentIDBox.Text);
                if (studentI==0)
                {
                    MessageBox.Show("An error occurred : Group ID can't be Null or 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                status = statustxt.Text;
                if (status == "Active")
                {
                    Status = 3;
                }
                else if (status == "InActive")
                {
                    Status = 4;
                }
                if (Adatetxt.Value == DateTime.Today.AddDays(1))
                {
                    MessageBox.Show("Date can't be NULL", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (Adatetxt.Value > DateTime.Today)
                {
                    MessageBox.Show("Please enter a date in Past or Present.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    AssignDate = Adatetxt.Value;
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("1"+ ex.Message);
                return;
            }
            if (!checkIfPresent(groupI,"Group")) 
            {
                MessageBox.Show("Group ID does not Exist", "Invalid GroupID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (checkifMemberComplete(groupI))
            {
                MessageBox.Show("Group's Limit Exceed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (checkifMatch(studentI))
            {
                MessageBox.Show("Group's Limit Exceed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!checkIfPresent(studentI, "Student"))
            {
                MessageBox.Show("Group ID does not Exist", "Invalid Student", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (groupANDstudentSame(groupI,studentI)) {
                MessageBox.Show("Student Already in this Group", "Invalid Student", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                string query = "INSERT INTO GroupStudent (GroupId,StudentId,Status,AssignmentDate) Values (@GroupId,@StudentId,@Status,@AssignmentDate)";
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@GroupId",groupI);
                    myCommand.Parameters.AddWithValue("@StudentId",studentI);
                    myCommand.Parameters.AddWithValue("@Status",Status);
                    myCommand.Parameters.AddWithValue("@AssignmentDate",AssignDate);  
                    myCommand.ExecuteNonQuery();
                }
                clearData();
                getfromDataBase();
                MessageBox.Show("Inserted");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void dltBTN_Click(object sender, EventArgs e)
        {
            string status;
            int Status = 0;
            DateTime? AssignDate = null;
            int groupI = 0, studentI = 0;
            String datetime = null;
            int ID = 0;
            try
            {
                groupI = int.Parse(grpIDbox.Text);
                if (groupI == 0)
                {
                    MessageBox.Show("An error occurred : Group ID can't be Null or 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                studentI = int.Parse(studentIDBox.Text);
                if (studentI == 0)
                {
                    MessageBox.Show("An error occurred : Group ID can't be Null or 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                status = statustxt.Text;
                if (status == "Active")
                {
                    Status = 3;
                }
                else if (status == "InActive")
                {
                    Status = 4;
                }
                if (Adatetxt.Value == DateTime.Today.AddDays(1))
                {
                    MessageBox.Show("Date can't be NULL", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (Adatetxt.Value > DateTime.Today)
                {
                    MessageBox.Show("Please enter a date in Past or Present.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    AssignDate = Adatetxt.Value;
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("1" + ex.Message);
                return;
            }
            if (!checkIfPresent(groupI, "Group"))
            {
                MessageBox.Show("Group ID does not Exist", "Invalid GroupID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            /*
            if (checkifMemberComplete(groupI))
            {
                MessageBox.Show("Group's Limit Exceed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!checkIfPresent(studentI, "Student"))
            {
                MessageBox.Show("Group ID does not Exist", "Invalid Student", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }*/
            if (!groupANDstudentSame(groupI, studentI))
            {
                MessageBox.Show("Student is Not in this Group", "Invalid Student", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                string query = "DELETE FROM GroupStudent WHERE GroupId = @GroupId AND StudentId = @StudentId";
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@GroupId", groupI);
                    myCommand.Parameters.AddWithValue("@StudentId", studentI);
                    myCommand.Parameters.AddWithValue("@Status", Status);
                    myCommand.Parameters.AddWithValue("@AssignmentDate", AssignDate);
                    myCommand.ExecuteNonQuery();
                }
                clearData();
                getfromDataBase();
                MessageBox.Show("Deleted");
            }
            catch (Exception ex)
            {
                Console.WriteLine("1" + ex.Message);
            }
        }
        private void BTNupdate_Click(object sender, EventArgs e)
        {
            string status;
            int Status = 0;
            DateTime? AssignDate = null;
            int groupI = 0, studentI = 0;
            String datetime = null;
            int ID = 0;
            try
            {
                groupI = int.Parse(grpIDbox.Text);
                if (groupI == 0)
                {
                    MessageBox.Show("An error occurred : Group ID can't be Null or 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                studentI = int.Parse(studentIDBox.Text);
                if (studentI == 0)
                {
                    MessageBox.Show("An error occurred : Group ID can't be Null or 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                status = statustxt.Text;
                if (status == "Active")
                {
                    Status = 3;
                }
                else if (status == "InActive")
                {
                    Status = 4;
                }
                if (Adatetxt.Value == DateTime.Today.AddDays(1))
                {
                    MessageBox.Show("Date can't be NULL", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (Adatetxt.Value > DateTime.Today)
                {
                    MessageBox.Show("Please enter a date in Past or Present.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    AssignDate = Adatetxt.Value;
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("1" + ex.Message);
                return;
            }
            if (!checkIfPresent(groupI, "Group"))
            {
                MessageBox.Show("Group ID does not Exist", "Invalid GroupID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!checkIfPresent(studentI, "Student"))
            {
                MessageBox.Show("Student ID does not Exist", "Invalid Student", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!groupANDstudentSame(groupI, studentI))
            {
                MessageBox.Show("Student is Not in this Group", "Invalid Student", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                string query = "UPDATE GroupStudent SET Status=@Status,AssignmentDate=@AssignmentDate WHERE GroupId=@GroupId AND StudentId=@StudentId";
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@GroupId", groupI);
                    myCommand.Parameters.AddWithValue("@StudentId", studentI);
                    myCommand.Parameters.AddWithValue("@Status", Status);
                    myCommand.Parameters.AddWithValue("@AssignmentDate", AssignDate);
                    myCommand.ExecuteNonQuery();
                }
                clearData();
                getfromDataBase();
                MessageBox.Show("Updated");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Hey " + ex.Message);
            }
        }
        private bool checkifMatch(int studentI)
        {
            int status = 3;
            SqlConnection connect = Configuration.getInstance().getConnection();
            {
                string query = "SELECT * FROM GroupStudent WHERE StudentId = @ID AND Status=@status";
                using (SqlCommand command = new SqlCommand(query, connect))
                {
                    command.Parameters.AddWithValue("@ID",studentI);
                    command.Parameters.AddWithValue("@status", status);
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
            }
        }
        private void datagrvw_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int grpid,studentid;
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = datagrvw.Rows[e.RowIndex];
                    grpid= Convert.ToInt32(row.Cells["GroupId"].Value);
                    grpIDbox.Text = grpid.ToString();
                    studentid = Convert.ToInt32(row.Cells["StudentId"].Value);
                    studentIDBox.Text = studentid.ToString();
                    statustxt.Text = (row.Cells["Status"].Value).ToString();
                    Adatetxt.Value = Convert.ToDateTime(row.Cells["AssignmentDate"].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        public void getfromDataBase()
        {
            try
            {
                int status = 3;
                var myCon = Configuration.getInstance().getConnection();
                string query = "SELECT * FROM GroupStudent WHERE  Status=@status";
                SqlCommand myCommand = new SqlCommand(query, myCon);
                myCommand.Parameters.AddWithValue("@status", status);
                SqlDataAdapter myAdapter = new SqlDataAdapter(myCommand);
                DataTable dt = new DataTable();
                myAdapter.Fill(dt);
                datagrvw.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private void clearData()
        { 
        try
            {
                studentIDBox.DataSource = studentIDs;
                grpIDbox.DataSource = groupIDs;
                statustxt.Text = "Active";
                Adatetxt.Value = DateTime.Today.AddDays(1);
                studentIDBox.Text = "";
                grpIDbox.Text = "";
            }
            catch (Exception ex) { }
        }
        private bool checkIfPresent(int ID, string tableName)
        {
            SqlConnection connect = Configuration.getInstance().getConnection();
            {
                string query = "SELECT * FROM [" + tableName + "] WHERE Id = @ID";
                using (SqlCommand command = new SqlCommand(query, connect))
                {
                    command.Parameters.AddWithValue("@ID", ID);
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
            }
        }
        private bool checkifMemberComplete(int ID)
        {
            
            SqlConnection connect = Configuration.getInstance().getConnection();
            {
                string query = "SELECT GroupId FROM GroupStudent WHERE GroupId = @ID GROUP BY GroupId HAVING COUNT(StudentId) > 4";
                using (SqlCommand command = new SqlCommand(query, connect))
                {
                    command.Parameters.AddWithValue("@ID", ID);
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return true;
                            }
                            else
                            {
                                Console.WriteLine("Hello");
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
            }
        }
        private void getfromDatabase2()
        {
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                string query = "SELECT * FROM [Group]";
                SqlCommand myCommand = new SqlCommand(query, myCon);
                SqlDataAdapter myAdapter = new SqlDataAdapter(myCommand);
                DataTable dt = new DataTable();
                myAdapter.Fill(dt);
                foreach (DataRow row in dt.Rows)
                {
                    int id = Convert.ToInt32(row["Id"]);
                    groupIDs.Add(id);
                }
                foreach (int id in groupIDs)
                {
                    Console.WriteLine(id);
                }
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private void getfromDatabase3()
        {
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                string query = "SELECT * FROM [Student]";
                SqlCommand myCommand = new SqlCommand(query, myCon);
                SqlDataAdapter myAdapter = new SqlDataAdapter(myCommand);
                DataTable dt = new DataTable();
                myAdapter.Fill(dt);
                foreach (DataRow row in dt.Rows)
                {
                    int id = Convert.ToInt32(row["Id"]);
                    studentIDs.Add(id);
                }
                foreach (int id in studentIDs)
                {
                    Console.WriteLine(id);
                }
                dataGridViewStudent.DataSource = dt;
               
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
          
        }
        private bool groupANDstudentSame(int groupI, int studentI) {
            SqlConnection connect = Configuration.getInstance().getConnection();
            {
                string query = "SELECT * FROM GroupStudent WHERE GroupId = @ID AND StudentId=@stID";
                using (SqlCommand command = new SqlCommand(query, connect))
                {
                    command.Parameters.AddWithValue("@ID",groupI);
                    command.Parameters.AddWithValue("@stID", studentI);
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
            }
        }
        private void clrBTN_Click(object sender, EventArgs e)
        {
            clearData();
        }
        private void bckBTN_Click(object sender, EventArgs e)
        {
            Program.FirstForm.Show();
            this.Close();
        }
        private void dataGridViewStudent_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void manage_GroupStudent_Load(object sender, EventArgs e)
        {

        }
    }
}
