﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Start_Project.Forms
{
    public partial class manage_Groups : Form
    {
        public manage_Groups()
        {
            InitializeComponent();
            getfromDataBase();
            clearData();
        }

        private void addBTN_Click(object sender, EventArgs e)
        {
            int ID = 0;
            string userInput = "";
            DateTime? selectedDate = null;
            try
            {
                selectedDate = createdOn.Value;
                DateTime today = DateTime.Today;
                if (selectedDate > today)
                {
                    MessageBox.Show("Please select a date on or before today's date.");
                    return;
                }
                try
                    {
                        var myConnect = Configuration.getInstance().getConnection();
                        string Query = "INSERT INTO [Group] (Created_On) Values (@date)";
                        using (SqlCommand connect = new SqlCommand(Query, myConnect))
                        {
                            connect.Parameters.AddWithValue("@date",selectedDate);
                            connect.ExecuteNonQuery();
                        }
                        getfromDataBase();
                        MessageBox.Show("Inserted");
                        clearData();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void clearData()
        {
            IDtxt.ReadOnly=true;
            IDtxt.Text = "ReadOnly";
            createdOn.Value = DateTime.Today;
        }

        private void datagrvw_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int id;
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = datagrvw.Rows[e.RowIndex];
                    id = Convert.ToInt32(row.Cells["Id"].Value);
                    IDtxt.Text = id.ToString();
                    DateTime selectedDate =Convert.ToDateTime(row.Cells["Created_On"].Value);
                    createdOn.Value = selectedDate;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void createdOn_ValueChanged(object sender, EventArgs e)
        {

        }
        public void getfromDataBase()
        {
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                string query = "SELECT * FROM [Group]";
                SqlCommand myCommand = new SqlCommand(query, myCon);
                SqlDataAdapter myAdapter = new SqlDataAdapter(myCommand);
                DataTable dt = new DataTable();
                myAdapter.Fill(dt);
                datagrvw.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void dltBTN_Click(object sender, EventArgs e)
        {
            int ID = 0;
            string userInput = "";
            DateTime? selectedDate = null;
            try
            {
                selectedDate = createdOn.Value;
                DateTime today = DateTime.Today;
                if (selectedDate > today)
                {
                    MessageBox.Show("Please select a date on or before today's date.");
                    return;
                }
                userInput = IDtxt.Text;
                if (string.IsNullOrWhiteSpace(userInput))
                {
                     MessageBox.Show("Avoid spaces and NULL in ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                     return;
                }
                else
                {
                     try
                     {
                         ID = int.Parse(userInput);
                     }
                     catch
                     {
                         MessageBox.Show("ID can't contain Letter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                         return;
                     }
                }
                try
                {
                    var myConnect = Configuration.getInstance().getConnection();
                    string Query = "DELETE FROM [Group] WHERE Id=@ID AND Created_On=@date";
                    using (SqlCommand connect = new SqlCommand(Query, myConnect))
                    {
                        connect.Parameters.AddWithValue("@date", selectedDate);
                        connect.Parameters.AddWithValue("@ID", ID);
                        connect.ExecuteNonQuery();
                    }
                    getfromDataBase();
                    MessageBox.Show("Deleted");
                    clearData();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void BTNupdate_Click(object sender, EventArgs e)
        {

            int ID = 0;
            string userInput = "";
            DateTime? selectedDate = null;
            try
            {
                selectedDate = createdOn.Value;
                DateTime today = DateTime.Today;
                if (selectedDate > today)
                {
                    MessageBox.Show("Please select a date on or before today's date.");
                    return;
                }
                userInput = IDtxt.Text;
                 if (string.IsNullOrWhiteSpace(userInput))
                 {
                     MessageBox.Show("Avoid spaces and NULL in ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                     return;
                 }
                 else
                 {
                     try
                     {
                         ID = int.Parse(userInput);
                     }
                     catch
                     {
                         MessageBox.Show("ID can't contain Letter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                         return;
                     }
                 }

                try
                {
                    var myConnect = Configuration.getInstance().getConnection();
                    string Query = "UPDATE [Group] SET Created_On=@date WHERE Id=@ID";
                    using (SqlCommand connect = new SqlCommand(Query, myConnect))
                    {
                        connect.Parameters.AddWithValue("@date", selectedDate);
                        connect.Parameters.AddWithValue("@ID",ID);
                        connect.ExecuteNonQuery();
                    }
                    getfromDataBase();
                    MessageBox.Show("Updated");
                    clearData();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void clrBTN_Click(object sender, EventArgs e)
        {
            clearData();
        }

        private void bckBTN_Click(object sender, EventArgs e)
        {

        }

        private void manage_Groups_Load(object sender, EventArgs e)
        {

        }
    }
}
