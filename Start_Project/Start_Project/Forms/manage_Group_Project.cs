﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Start_Project.Forms
{
    public partial class manage_Group_Project : Form
    {
        List<int> prjctIDs = new List<int>();
        List<int> groupIDs = new List<int>();
        public manage_Group_Project()
        {
            InitializeComponent();
        }
        private void datagrvw_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int grpid, ProjectId;
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = datagrvw.Rows[e.RowIndex];
                    grpid = Convert.ToInt32(row.Cells["GroupId"].Value);
                    grpIDbox.Text = grpid.ToString();
                    ProjectId = Convert.ToInt32(row.Cells["ProjectId"].Value);
                    prjctIDBox.Text = ProjectId.ToString();
                    Adatetxt.Value = Convert.ToDateTime(row.Cells["AssignmentDate"].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void manage_Group_Project_Load(object sender, EventArgs e)
        {
            getfromDatabase();
            getfromDatabase1();
            getfromDatabase2();
            clearData();
        }
        private void clearData()
        {
            try
            {
                prjctIDBox.DataSource = prjctIDs;
                grpIDbox.DataSource = groupIDs;
                Adatetxt.Value = DateTime.Today.AddDays(1);
                prjctIDBox.Text = "";
                grpIDbox.Text = "";
            }
            catch (Exception ex) { }
        }
        private void getfromDatabase2()
        {
            var myCon = Configuration.getInstance().getConnection();
            string query = "SELECT Id FROM [Group]";
            SqlCommand myCommand = new SqlCommand(query, myCon);
            SqlDataAdapter myDataAdapter = new SqlDataAdapter(myCommand);
            DataTable dt = new DataTable();
            myDataAdapter.Fill(dt);
            dataGridViewgrp.DataSource = dt;
            foreach (DataRow row in dt.Rows)
            {
                int id = Convert.ToInt32(row["Id"]);
                groupIDs.Add(id);
            }
        }
        private void getfromDatabase1()
        {
            var myCon = Configuration.getInstance().getConnection();
            string query = "SELECT Id FROM Project";
            SqlCommand myCommand = new SqlCommand(query, myCon);
            SqlDataAdapter myDataAdapter = new SqlDataAdapter(myCommand);
            DataTable dt = new DataTable();
            myDataAdapter.Fill(dt);
            dataGridViewprjct.DataSource = dt;
            foreach (DataRow row in dt.Rows)
            {
                int id = Convert.ToInt32(row["Id"]);
                prjctIDs.Add(id);
            }
        }
        private void getfromDatabase()
        {
            var myCon = Configuration.getInstance().getConnection();
            string query = "SELECT * FROM GroupProject";
            SqlCommand myCommand = new SqlCommand(query, myCon);
            SqlDataAdapter myDataAdapter = new SqlDataAdapter(myCommand);
            DataTable dt = new DataTable();
            myDataAdapter.Fill(dt);
            datagrvw.DataSource = dt;
        }
        private void dataGridViewprjct_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void addBTN_Click(object sender, EventArgs e)
        {
            DateTime? AssignDate = null;
            int groupI = 0, prjctI = 0;
            String datetime = null;
            int ID = 0;
            try
            {
                groupI = int.Parse(grpIDbox.Text);
                if (groupI == 0)
                {
                    MessageBox.Show("An error occurred : Group ID can't be Null or 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                prjctI = int.Parse(prjctIDBox.Text);
                if (prjctI == 0)
                {
                    MessageBox.Show("An error occurred : Project ID can't be Null or 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (Adatetxt.Value == DateTime.Today.AddDays(1))
                {
                    MessageBox.Show("Date can't be NULL", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (Adatetxt.Value > DateTime.Today)
                {
                    MessageBox.Show("Please enter a date in Past or Present.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    AssignDate = Adatetxt.Value;
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("1" + ex.Message);
                return;
            }
            if (!checkIfPresent(groupI, "Group"))
            {
                MessageBox.Show("Group ID does not Exist", "Invalid GroupID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!checkIfPresent(prjctI, "Project"))
            {
                MessageBox.Show("Project ID does not Exist", "Invalid Student", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (checkifMatch(prjctI,groupI))
            {
                MessageBox.Show("Group Already have Project", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (checkifMatch(prjctI,"ProjectId"))
            {
                MessageBox.Show("Project Already Assigned", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            } 
            else if(checkifMatch(groupI, "GroupId"))
            {
                MessageBox.Show("Group Existed already", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                Console.Write("Hello");
                string query = "INSERT INTO GroupProject (ProjectId,GroupId,AssignmentDate) Values (@ProjectId,@GroupId,@AssignmentDate)";
                
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@GroupId", groupI);
                    myCommand.Parameters.AddWithValue("@ProjectId", prjctI);
                    myCommand.Parameters.AddWithValue("@AssignmentDate", AssignDate);
                    myCommand.ExecuteNonQuery();
                }
                clearData();
                getfromDatabase();
                MessageBox.Show("Inserted");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private bool checkifMatch(int PId,int grpId)
        {
            SqlConnection connect = Configuration.getInstance().getConnection();
            {
                string query = "SELECT * FROM GroupProject WHERE ProjectId = @ID AND GroupId=@grpId";
                using (SqlCommand command = new SqlCommand(query, connect))
                {
                    command.Parameters.AddWithValue("@ID",PId);
                    command.Parameters.AddWithValue("@grpId", grpId);
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
            }
        }
        private bool checkIfPresent(int ID, string tableName)
        {
            SqlConnection connect = Configuration.getInstance().getConnection();
            {
                string query = "SELECT * FROM [" + tableName + "] WHERE Id = @ID";
                using (SqlCommand command = new SqlCommand(query, connect))
                {
                    command.Parameters.AddWithValue("@ID", ID);
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
            }
        }
        private bool checkifMatch(int id,string name)
        {
            SqlConnection connect = Configuration.getInstance().getConnection();
            {
                string query = "SELECT * FROM GroupProject WHERE "+name+" = @ID";
                using (SqlCommand command = new SqlCommand(query, connect))
                {
                    command.Parameters.AddWithValue("@ID", id);
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
            }
        }
        private void dltBTN_Click(object sender, EventArgs e)
        {
            DateTime? AssignDate = null;
            int groupI = 0, prjctI = 0;
            String datetime = null;
            int ID = 0;
            try
            {
                groupI = int.Parse(grpIDbox.Text);
                if (groupI == 0)
                {
                    MessageBox.Show("An error occurred : Group ID can't be Null or 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                prjctI = int.Parse(prjctIDBox.Text);
                if (prjctI == 0)
                {
                    MessageBox.Show("An error occurred : Project ID can't be Null or 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("1" + ex.Message);
                return;
            }
            if (!checkIfPresent(groupI, "Group"))
            {
                MessageBox.Show("Group ID does not Exist", "Invalid GroupID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!checkIfPresent(prjctI, "Project"))
            {
                MessageBox.Show("Project ID does not Exist", "Invalid Student", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!checkifMatch(prjctI, groupI))
            {
                MessageBox.Show("Group does not Exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                Console.Write("Hello");
                string query = "DELETE FROM GroupProject WHERE ProjectId=@ProjectId AND GroupId=@GroupId";

                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@GroupId", groupI);
                    myCommand.Parameters.AddWithValue("@ProjectId", prjctI);
                    myCommand.ExecuteNonQuery();
                }
                clearData();
                getfromDatabase();
                MessageBox.Show("Deleted");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void BTNupdate_Click(object sender, EventArgs e)
        {
            DateTime? AssignDate = null;
            int groupI = 0, prjctI = 0;
            String datetime = null;
            int ID = 0;
            try
            {
                groupI = int.Parse(grpIDbox.Text);
                if (groupI == 0)
                {
                    MessageBox.Show("An error occurred : Group ID can't be Null or 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                prjctI = int.Parse(prjctIDBox.Text);
                if (prjctI == 0)
                {
                    MessageBox.Show("An error occurred : Project ID can't be Null or 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (Adatetxt.Value == DateTime.Today.AddDays(1))
                {
                    MessageBox.Show("Date can't be NULL", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (Adatetxt.Value > DateTime.Today)
                {
                    MessageBox.Show("Please enter a date in Past or Present.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    AssignDate = Adatetxt.Value;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("1" + ex.Message);
                return;
            }
            if (!checkIfPresent(groupI, "Group"))
            {
                MessageBox.Show("Group ID does not Exist", "Invalid GroupID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.Write("Hello");
                return;
            }
            if (!checkIfPresent(prjctI, "Project"))
            {
                MessageBox.Show("Project ID does not Exist", "Invalid Student", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }           
            if (!checkifMatch(prjctI, "ProjectId"))
            {
                MessageBox.Show("Project does not Exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (!checkifMatch(groupI, "GroupId"))
            {
                MessageBox.Show("Group does not Exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.Write("Hello 3");
                return;
            }
            if (!checkifMatch(prjctI, groupI))
            {
                MessageBox.Show(" Group does not Exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.Write("Hello 1");
                return;
            }
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                Console.Write("Hello");
                string query = "UPDATE GroupProject SET AssignmentDate=@AssignmentDate WHERE ProjectId=@ProjectId AND GroupId=@GroupId";

                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@GroupId", groupI);
                    myCommand.Parameters.AddWithValue("@ProjectId", prjctI);
                    myCommand.Parameters.AddWithValue("@AssignmentDate", AssignDate);
                    myCommand.ExecuteNonQuery();
                }
                clearData();
                getfromDatabase();
                MessageBox.Show("Updated");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void clrBTN_Click(object sender, EventArgs e)
        {
            clearData();
        }
        private void bckBTN_Click(object sender, EventArgs e)
        {
            Program.FirstForm.Show();
            this.Close();
        }
    }
}
