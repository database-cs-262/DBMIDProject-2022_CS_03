﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Start_Project.Forms
{
    public partial class manage_Evaluations : Form
    {
        public manage_Evaluations()
        {
            InitializeComponent();
        }
        private void addBTN_Click(object sender, EventArgs e)
        {
            string userInput = null;
            string name = null;
            string Tmarks = null;
            int tMarks = 0;
            string Tweight = null;
            int tWeight = 0;
            try
            {
                name = NAMEtxt.Text;
                Tweight = Weightagetxt.Text;
                Tmarks = TotalMarkstxt.Text;
                if (string.IsNullOrWhiteSpace(name))
                {
                    MessageBox.Show("Avoid spaces and NULL in Name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (name.Length > 200)
                {
                    MessageBox.Show("Name exceeds Limit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (string.IsNullOrWhiteSpace(Tweight))
                {
                    MessageBox.Show("Avoid spaces and NULL in Total Weightage", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (string.IsNullOrWhiteSpace(Tmarks))
                {
                    MessageBox.Show("Avoid spaces and NULL in Marks", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                try
                {
                    try
                    {
                        tMarks = int.Parse(Tmarks);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Marks can't contain Strings", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Console.WriteLine("4");
                        return;
                    }
                    if (tMarks < 1)
                    {
                        MessageBox.Show("Marks can't be Negative", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    try
                    {
                        tWeight = int.Parse(Tweight);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Weightage can't contain Strings", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (tWeight < 1)
                    {
                        MessageBox.Show("Weightage can't be NULL", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (ifDuplicateExist(name))
                    {
                        DialogResult result = MessageBox.Show("Name already Exist", "Confirmation", MessageBoxButtons.YesNo,MessageBoxIcon.Exclamation);
                        if (result == DialogResult.No)
                        {
                            return;
                        }
                    }
                    try
                    {
                        var myConnect = Configuration.getInstance().getConnection();
                        string Query = "INSERT INTO Evaluation (Name,TotalMarks,TotalWeightage) Values (@name,@tMarks,@tWeight)";
                        using (SqlCommand connect = new SqlCommand(Query, myConnect))
                        {
                            connect.Parameters.AddWithValue("@name", name);
                            connect.Parameters.AddWithValue("@tMarks", tMarks);
                            connect.Parameters.AddWithValue("@tWeight", tWeight);
                            connect.ExecuteNonQuery();
                        }
                        getfromDataBase();
                        clearData();
                        MessageBox.Show("Inserted");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private bool ifDuplicateExist(string name)
        {
            SqlConnection connect = Configuration.getInstance().getConnection();
            string query = "SELECT * FROM Evaluation WHERE Name = @name";
            using (SqlCommand command = new SqlCommand(query, connect))
            {
                command.Parameters.AddWithValue("@name",name);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    try
                    {
                        if (reader.Read())
                        {
                            return true;
                        }
                    }
                    catch (Exception ex) { Console.WriteLine(ex.Message); }
                }
            }
            return false;
        }
        private void manage_Evaluations_Load(object sender, EventArgs e)
        {
            clrBTN_Click(sender, e);
            getfromDataBase();
        }
        public void getfromDataBase()
        {
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                string query = "SELECT * FROM Evaluation";
                SqlCommand myCommand = new SqlCommand(query, myCon);
                SqlDataAdapter myAdapter = new SqlDataAdapter(myCommand);
                DataTable dt = new DataTable();
                myAdapter.Fill(dt);
                datagrvw.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private void datagrvw_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int id;
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = datagrvw.Rows[e.RowIndex];
                    id = Convert.ToInt32(row.Cells["Id"].Value);
                    IDtxt.Text = id.ToString();
                    NAMEtxt.Text = (row.Cells["Name"].Value).ToString();
                    TotalMarkstxt.Text = (row.Cells["TotalMarks"].Value).ToString();
                    Weightagetxt.Text = (row.Cells["TotalWeightage"].Value).ToString();
                    IDtxt.ReadOnly = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void BTNupdate_Click(object sender, EventArgs e)
        {
            string userInput = null;
            int ID=0;
            string name = null;
            string Tmarks = null;
            int tMarks = 0;
            string Tweight = null;
            int tWeight = 0;
            try
            {
                userInput = IDtxt.Text; 
                name = NAMEtxt.Text;
                Tweight = Weightagetxt.Text;
                Tmarks = TotalMarkstxt.Text;
                if (string.IsNullOrWhiteSpace(userInput))
                {
                    Console.WriteLine("5");
                    MessageBox.Show("Avoid spaces and NULL in ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                try 
                { 
                    ID = int.Parse(userInput);
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                if (string.IsNullOrWhiteSpace(name))
                {
                    Console.WriteLine("6");
                    MessageBox.Show("Avoid spaces and NULL in Name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (name.Length > 200)
                {
                    MessageBox.Show("Name exceeds Limit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (string.IsNullOrWhiteSpace(Tweight))
                {
                    MessageBox.Show("Avoid spaces and NULL in Total Weightage", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            if (string.IsNullOrWhiteSpace(Tmarks))
            {
                    MessageBox.Show("Avoid spaces and NULL in Marks", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            }
            try
            { 
                 try
                 {
                    tMarks = int.Parse(Tmarks);
                 }
            catch (Exception ex)
            {
                MessageBox.Show("Marks can't contain Strings", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("4");
                return;
            }
                    if (tMarks < 1)
                    {
                        MessageBox.Show("Marks can't be NULL", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
            try
            {
                tWeight = int.Parse(Tweight);
            }
            catch (Exception ex)
            {
                Console.WriteLine("3");
                MessageBox.Show("Weightage can't contain Strings", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
                    if (tWeight < 1)
                    {
                        MessageBox.Show("Weightage can't be NULL", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
            if (!checkIfPresent(ID))
            {
                        MessageBox.Show("Not Found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
            }
                    if (ifDuplicateExist(name))
                    {
                        DialogResult result = MessageBox.Show("Name already Exist", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                        if (result == DialogResult.No)
                        {
                            return;
                        }
                    }
                    try
            {
                var myConnect = Configuration.getInstance().getConnection();
                string Query = "UPDATE Evaluation SET Name=@name,TotalMarks=@tMarks,TotalWeightage=@tWeight Where Id=@ID";
                using (SqlCommand connect = new SqlCommand(Query, myConnect))
                {
                    connect.Parameters.AddWithValue("@ID",ID);
                    connect.Parameters.AddWithValue("@name", name);
                    connect.Parameters.AddWithValue("@tMarks", tMarks);
                    connect.Parameters.AddWithValue("@tWeight", tWeight);
                    connect.ExecuteNonQuery();
                }
                        getfromDataBase();
                MessageBox.Show("Updated");

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }
                catch (Exception ex)
                {
                    Console.WriteLine("2");
                    Console.WriteLine(ex.Message);
                }

}
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void dltBTN_Click(object sender, EventArgs e)
        {
            string userInput = null;
            int ID = 0;
            string name = null;
            string Tmarks = null;
            int tMarks = 0;
            string Tweight = null;
            int tWeight = 0;
            try
            {
                userInput = IDtxt.Text;
                name = NAMEtxt.Text;
                Tweight = Weightagetxt.Text;
                Tmarks = TotalMarkstxt.Text;
                if (string.IsNullOrWhiteSpace(userInput))
                {
                    Console.WriteLine("5");
                    MessageBox.Show("Avoid spaces and NULL in ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                try
                {
                    ID = int.Parse(userInput);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                if (string.IsNullOrWhiteSpace(name))
                {
                    Console.WriteLine("6");
                    MessageBox.Show("Avoid spaces and NULL in Name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (name.Length > 200)
                {
                    MessageBox.Show("Name exceeds Limit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (string.IsNullOrWhiteSpace(Tweight))
                {
                    MessageBox.Show("Avoid spaces and NULL in Total Weightage", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (string.IsNullOrWhiteSpace(Tmarks))
                {
                    MessageBox.Show("Avoid spaces and NULL in Marks", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                try
                {
                    try
                    {
                        tMarks = int.Parse(Tmarks);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Marks can't contain Strings", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Console.WriteLine("4");
                        return;
                    }
                    try
                    {
                        tWeight = int.Parse(Tweight);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("3");
                        MessageBox.Show("Weightage can't contain Strings", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (!checkIfPresent(ID))
                    {
                        MessageBox.Show("Not Found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    try
                    {
                        var myConnect = Configuration.getInstance().getConnection();
                        string Query = "DELETE FROM Evaluation Where Id=@ID AND Name=@name";
                        using (SqlCommand connect = new SqlCommand(Query, myConnect))
                        {
                            connect.Parameters.AddWithValue("@ID", ID);
                            connect.Parameters.AddWithValue("@name", name);
                            connect.ExecuteNonQuery();
                        }
                        getfromDataBase();
                        clearData();
                        MessageBox.Show("Deleted");
                    }
                    catch (Exception ex)
                    {

                        Console.WriteLine(ex.Message);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("2");
                    Console.WriteLine(ex.Message);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        void clearData()
        {
            IDtxt.Text = "";
            NAMEtxt.Text = "";
            TotalMarkstxt.Text = "";
            Weightagetxt.Text = "";
        }
        private void clrBTN_Click(object sender, EventArgs e)
        {
            IDtxt.ReadOnly = true;
            clearData();
        }
        private bool checkIfPresent(int ID)
        {
            SqlConnection connect = Configuration.getInstance().getConnection();
            string query = "SELECT * FROM Evaluation WHERE Id = @ID";
            using (SqlCommand command = new SqlCommand(query, connect))
            {
                command.Parameters.AddWithValue("@ID", ID);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    try
                    {
                        if (!reader.Read())
                        {
                            Console.WriteLine("hE");
                            return false;
                        }
                    }
                    catch (Exception ex) { Console.WriteLine(ex.Message); }
                }
            }
            return true;
        }
        private void bckBTN_Click(object sender, EventArgs e)
        {
            Program.FirstForm.Show();
            this.Close();
        }
    }
}
