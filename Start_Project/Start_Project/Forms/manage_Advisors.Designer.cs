﻿namespace Start_Project.Forms
{
    partial class manage_Advisors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crystalReport41 = new Start_Project.CrystalReport4();
            this.SuspendLayout();
            // 
            // crystalReport41
            // 
            this.crystalReport41.InitReport += new System.EventHandler(this.crystalReport41_InitReport);
            // 
            // manage_Advisors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Name = "manage_Advisors";
            this.Text = "manage_Advisors";
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalReport4 crystalReport41;
    }
}