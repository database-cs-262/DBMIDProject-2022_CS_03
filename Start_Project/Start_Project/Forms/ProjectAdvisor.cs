﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Start_Project.Forms
{
    public partial class ProjectAdvisor : Form
    {
        List<int> advisorId = new List<int>();
        List<int> ProjectIds = new List<int>();
        public ProjectAdvisor()
        {
            InitializeComponent();
            
        }
        private void ProjectAdvisor_Load(object sender, EventArgs e)
        {
            getfromDatabase1();
            getfromDatabase2();
            getfromDatabase();
            clearData();
        }
        private void addBTN_Click(object sender, EventArgs e)
        {
            DateTime? AssignDate = null;
            int advisorI = 0, prjctI = 0;
            String datetime = null;
            string desig = null;
            int desigI = 0;
            int ID = 0;
            try
            {
                desig = DesigBox.Text;
                if (!string.IsNullOrEmpty(Advisorbox.Text))
                {
                    advisorI = int.Parse(Advisorbox.Text);
                }
                if (advisorI == 0)
                {
                    MessageBox.Show("An error occurred : Advisor ID can't be  0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (!string.IsNullOrEmpty(prjctIDBox.Text))
                { 
                    prjctI = int.Parse(prjctIDBox.Text);
                }
                if (prjctI == 0)
                {
                    MessageBox.Show("An error occurred : Project ID can't be Null or 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (Adatetxt.Value == DateTime.Today.AddDays(1))
                {
                    MessageBox.Show("Date can't be NULL", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (Adatetxt.Value > DateTime.Today)
                {
                    MessageBox.Show("Please enter a date in Past or Present.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    AssignDate = Adatetxt.Value;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("1" + ex.Message);
                return;
            }
            if (!checkIfPresent(advisorI, "Advisor"))
            {
                MessageBox.Show("Advisor ID does not Exist", "Invalid AdvisorId", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Console.WriteLine(checkIfPresent(prjctI, "Project"));
            if (!checkIfPresent(prjctI, "Project"))
            {
                Console.WriteLine("121");
                MessageBox.Show("Project ID does not Exist", "Invalid Project", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (checkifMatch(prjctI, advisorI))
            {
                MessageBox.Show("Already Assigned", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (checkCountofAdvisors(prjctI))
            {
                MessageBox.Show("Already Assigned 3", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Console.WriteLine(desig);
            if (desig == "Main Advisor")
            {
                desigI = 11;
            }
            else if (desig == "Co-Advisor")
            {
                desigI = 12;
            }
            else if (desig == "Industry Advisor")
            {
                desigI = 14;
            }
            if (checkDuplicationofRole(prjctI, desigI))
            {
                MessageBox.Show("Already Assigned: "+desig+"", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }            
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                Console.Write("Hello");
                string query = "INSERT INTO ProjectAdvisor (AdvisorId,ProjectId,AdvisorRole,AssignmentDate) Values (@AdvisorId,@ProjectId,@advRole,@AssignmentDate)";

                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@AdvisorId", advisorI);
                    myCommand.Parameters.AddWithValue("@ProjectId", prjctI);
                    myCommand.Parameters.AddWithValue("@AdvRole",  desigI);
                    myCommand.Parameters.AddWithValue("@AssignmentDate", AssignDate);
                    myCommand.ExecuteNonQuery();
                }
                clearData();
                getfromDatabase();
                MessageBox.Show("Inserted");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private bool checkifMatch(int PId, int advId)
        {
            SqlConnection connect = Configuration.getInstance().getConnection();
            {
                string query = "SELECT * FROM ProjectAdvisor WHERE ProjectId = @ID AND AdvisorId=@advId";
                using (SqlCommand command = new SqlCommand(query, connect))
                {
                    command.Parameters.AddWithValue("@ID", PId);
                    command.Parameters.AddWithValue("@advId", advId);
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
            }
        }
        private bool checkIfPresent(int ID, string tableName)
        {
            SqlConnection connect = Configuration.getInstance().getConnection();
            {
                string query = "SELECT * FROM [" + tableName + "] WHERE Id = @ID";
                using (SqlCommand command = new SqlCommand(query, connect))
                {
                    command.Parameters.AddWithValue("@ID", ID);
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
            }
        }
        private bool checkDuplicationofRole(int ID,int roleT)
        {
            SqlConnection connect = Configuration.getInstance().getConnection();
            {
                string query = "SELECT ProjectId FROM ProjectAdvisor WHERE ProjectId = @ID AND AdvisorRole=@Rola";
                using (SqlCommand command = new SqlCommand(query, connect))
                {
                    command.Parameters.AddWithValue("@ID", ID);
                    command.Parameters.AddWithValue("@Rola", roleT);
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
            }
        }
        private bool checkCountofAdvisors(int ID)
        {
            SqlConnection connect = Configuration.getInstance().getConnection();
            {
                string query = "SELECT ProjectId FROM ProjectAdvisor WHERE ProjectId = @ID Group BY ProjectId Having COUNT(AdvisorId)>2";
                using (SqlCommand command = new SqlCommand(query, connect))
                {
                    command.Parameters.AddWithValue("@ID", ID);
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
            }
        }

        private bool checkifMatch(int id, string name)
        {
            SqlConnection connect = Configuration.getInstance().getConnection();
            {
                string query = "SELECT * FROM GroupProject WHERE " + name + " = @ID";
                using (SqlCommand command = new SqlCommand(query, connect))
                {
                    command.Parameters.AddWithValue("@ID", id);
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
            }
        }
        private void clearData()
        {
            try
            {
                Advisorbox.DataSource = advisorId;
                prjctIDBox.DataSource = ProjectIds;
                Adatetxt.Value = DateTime.Today.AddDays(1);
                prjctIDBox.Text = "";
                prjctIDBox.Text = "";
            }
            catch (Exception ex) { }
        }
        private void getfromDatabase2()
        {
            var myCon = Configuration.getInstance().getConnection();
            string query = "SELECT Id FROM [Group]";
            SqlCommand myCommand = new SqlCommand(query, myCon);
            SqlDataAdapter myDataAdapter = new SqlDataAdapter(myCommand);
            DataTable dt = new DataTable();
            myDataAdapter.Fill(dt);
            dataGridViewgrp.DataSource = dt;
            foreach (DataRow row in dt.Rows)
            {
                int id = Convert.ToInt32(row["Id"]);
                ProjectIds.Add(id);
            }
        }
        private void getfromDatabase1()
        {
            var myCon = Configuration.getInstance().getConnection();
            string query = "SELECT Id FROM Advisor";
            SqlCommand myCommand = new SqlCommand(query, myCon);
            SqlDataAdapter myDataAdapter = new SqlDataAdapter(myCommand);
            DataTable dt = new DataTable();
            myDataAdapter.Fill(dt);
            dataGridViewprjct.DataSource = dt;
            foreach (DataRow row in dt.Rows)
            {
                int id = Convert.ToInt32(row["Id"]);
                advisorId.Add(id);
            }
        }
        private void getfromDatabase()
        {
            var myCon = Configuration.getInstance().getConnection();
            string query = "SELECT * FROM ProjectAdvisor";
            SqlCommand myCommand = new SqlCommand(query, myCon);
            SqlDataAdapter myDataAdapter = new SqlDataAdapter(myCommand);
            DataTable dt = new DataTable();
            myDataAdapter.Fill(dt);
            datagrvw.DataSource = dt;
        }
        private void datagrvw_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int advId, ProjectId;
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = datagrvw.Rows[e.RowIndex];
                    advId = Convert.ToInt32(row.Cells["AdvisorId"].Value);
                    Advisorbox.Text = advId.ToString();
                    ProjectId = Convert.ToInt32(row.Cells["ProjectId"].Value);
                    prjctIDBox.Text = ProjectId.ToString();
                    Adatetxt.Value = Convert.ToDateTime(row.Cells["AssignmentDate"].Value.ToString());
                    int desig=Convert.ToInt32((row.Cells["AdvisorRole"].Value).ToString());
                    if (desig== 11)
                    {
                     
                        DesigBox.Text = "Main Advisor";
                    }
                    else if (desig == 11)
                    {

                        DesigBox.Text = "Co-Advisor";
                    }else if (desig == 14)
                    {
                        DesigBox.Text = "Industry Advisor";
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void Advisorbox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void bckBTN_Click(object sender, EventArgs e)
        {
            Program.FirstForm.Show();
            this.Close();
        }
        private void clrBTN_Click(object sender, EventArgs e)
        {
            clearData();
        }
        private void dltBTN_Click(object sender, EventArgs e)
        {
            DateTime? AssignDate = null;
            int advisorI = 0, prjctI = 0;
            String datetime = null;
            string desig = null;
            int desigI = 0;
            int ID = 0;
            try
            {
                desig = DesigBox.Text;
                if (!string.IsNullOrEmpty(Advisorbox.Text))
                {
                    advisorI = int.Parse(Advisorbox.Text);
                }
                if (advisorI == 0)
                {
                    MessageBox.Show("An error occurred : Advisor ID can't be  0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (!string.IsNullOrEmpty(prjctIDBox.Text))
                {
                    prjctI = int.Parse(prjctIDBox.Text);
                }
                if (prjctI == 0)
                {
                    MessageBox.Show("An error occurred : Project ID can't be Null or 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (Adatetxt.Value == DateTime.Today.AddDays(1))
                {
                    MessageBox.Show("Date can't be NULL", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (Adatetxt.Value > DateTime.Today)
                {
                    MessageBox.Show("Please enter a date in Past or Present.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    AssignDate = Adatetxt.Value;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("1" + ex.Message);
                return;
            }
            if (!checkIfPresent(advisorI, "Advisor"))
            {
                MessageBox.Show("Advisor ID does not Exist", "Invalid AdvisorId", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!checkIfPresent(prjctI, "Project"))
            {
                Console.WriteLine("121");
                MessageBox.Show("Project ID does not Exist", "Invalid Project", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!checkifMatch(prjctI, advisorI))
            {
                MessageBox.Show("No Record", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (desig == "Main Advisor")
            {
                desigI = 11;
            }
            else if (desig == "Co-Advisor")
            {
                desigI = 12;
            }
            else if (desig == "Industry Advisor")
            {
                desigI = 14;
            }
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                Console.Write("Hello");
                string query = "DELETE FROM ProjectAdvisor WHERE AdvisorId=@AdvisorId AND ProjectId=@ProjectId";

                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@AdvisorId", advisorI);
                    myCommand.Parameters.AddWithValue("@ProjectId", prjctI);
                    myCommand.ExecuteNonQuery();
                }
                clearData();
                getfromDatabase();
                MessageBox.Show("Deleted");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
