﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;


namespace Start_Project.Forms
{
    public partial class manage_Projects : Form
    {
        bool clr = true;
        public manage_Projects()
        {
            InitializeComponent();
        }
        private void manage_Projects_Load(object sender, EventArgs e)
        {
            getfromDataBase();
        }
        private void addBTN_Click(object sender, EventArgs e)
        {
            idTxt.Text = "";
            string Title = null;
            string Description = null;
            //int ID;
            try
            {
                Title = titletxt.Text;
                if (Title == null || Title.Length == 0 || Title.Length > 50)
                {
                    MessageBox.Show("An error occurred : Title can't be NULL \nMust be smaller than 50", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                Description = Descriptiontxt.Text;
                if (Description == "")
                {
                    Description = null;
                }
            }
            catch (Exception ex)
            {
                Console.Write("1" + ex.ToString());
            }
            if (checkIfPresent(Title))
            {
                MessageBox.Show("An error occurred : Title exist Already", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                string query = "INSERT INTO Project Values (@Description,@Title)";
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    if (Title == null)
                    { 
                        return;
                    }
                    else 
                    {
                        myCommand.Parameters.AddWithValue("@Title", Title);
                    }
                    if (Description == null)
                    {
                        myCommand.Parameters.AddWithValue("@Description", DBNull.Value);
                    }
                    else
                    {
                        myCommand.Parameters.AddWithValue("@Description", Description);
                    }
                    myCommand.ExecuteNonQuery();
                }
                clearData();
                getfromDataBase();
                MessageBox.Show("Inserted");
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Hello: \n" + Title);
                Console.WriteLine(ex.Message);
            }
        }
        private void Descriptiontxt_TextChanged(object sender, EventArgs e)
        {

        }
        private void titletxt_TextChanged(object sender, EventArgs e)
        {

        }
        private void BTNupdate_Click(object sender, EventArgs e)
        {
            string Title = null;
            string Description = null;
            int ID = 0;
            string id=idTxt.Text;
            try
            {
                    if (string.IsNullOrEmpty(id))
                    {
                        MessageBox.Show("ID can't be Empty ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    try
                    {
                        ID = int.Parse(id);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ID must be of INT type", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    Title = titletxt.Text;
                    if (Title == null || Title.Length == 0 || Title.Length > 50)
                    {
                        MessageBox.Show("An error occurred : Title can't be NULL \nMust be smaller than 50", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    Description = Descriptiontxt.Text;
                    if (Description == "")
                    {
                        Description = null;
                    }
            }
            catch (Exception ex)
            {
                    Console.Write(ex.ToString());
                    return;
            }
            if (!checkIfPresent(ID) && !(checkIfPresent(Title)))
            {
                MessageBox.Show("Not Found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                int rowsAffected = 0;
                    var myCon = Configuration.getInstance().getConnection();
                    string query = "UPDATE Project SET Description=@Description,Title=@Title WHERE Id=@ID";
                    using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    {
                         myCommand.Parameters.AddWithValue("@Title", Title);
                        myCommand.Parameters.AddWithValue("@ID", ID);
                        if (Description == null)
                        {
                            myCommand.Parameters.AddWithValue("@Description", DBNull.Value);
                        }
                        else
                        {
                            Console.WriteLine("text");
                            myCommand.Parameters.AddWithValue("@Description", Description);
                        }
                    rowsAffected = myCommand.ExecuteNonQuery();
                }
                if (rowsAffected > 0)
                {
                    MessageBox.Show("Updated");
                }
                else
                {
                    MessageBox.Show("ID and Title are of Different Projects");
                }
                clearData();
                    clr = true;
                getfromDataBase();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
           
        }
        private void idTxt_TextChanged(object sender, EventArgs e)
        {

        }
        private void clearData()
        {
            try
            {
                idTxt.Text = "Auto";
                idTxt.ReadOnly = true;
                Descriptiontxt.Text = "";
                titletxt.Text = "";
            }
            catch (Exception ex) { }

        }
        public void getfromDataBase()
        {
            try
            {
                var myCon = Configuration.getInstance().getConnection();
                string query = "SELECT * FROM Project";
                SqlCommand myCommand = new SqlCommand(query, myCon);
                SqlDataAdapter myAdapter = new SqlDataAdapter(myCommand);
                DataTable dt = new DataTable();
                myAdapter.Fill(dt);
                datagrvw.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private void clrBTN_Click(object sender, EventArgs e)
        {
            clr = true;
            clearData();
        }       
        private bool checkIfPresent(int ID)
        {
                SqlConnection connect = Configuration.getInstance().getConnection();
                string query = "SELECT * FROM Project WHERE Id = @ID";
                using (SqlCommand command = new SqlCommand(query, connect))
                {
                    command.Parameters.AddWithValue("@ID", ID);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            if (reader.Read())
                            {
                            return true;
                            }
                        }
                        catch (Exception ex) { Console.WriteLine(ex.Message); }
                    }

                }
                return false;
        }
        private bool checkIfPresent(string Title)
        {
            SqlConnection connect = Configuration.getInstance().getConnection();
            string query = "SELECT * FROM Project WHERE Title = @title";
            using (SqlCommand command = new SqlCommand(query, connect))
            {
                command.Parameters.AddWithValue("@title",Title);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    try
                    {
                        if (reader.Read())
                        {
                            return true;
                        }
                    }
                    catch (Exception ex) { Console.WriteLine(ex.Message); }
                }

            }
            return false;
        }
        private void dltBTN_Click(object sender, EventArgs e)
        {
            string Title = null;
            int ID = 0;
            string id = idTxt.Text;
            try
            {
                        if (string.IsNullOrEmpty(id))
                        {
                            MessageBox.Show("ID can't be Empty ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        try
                        {
                            ID = int.Parse(id);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ID must be of INT type", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        Title = titletxt.Text;
                        if (Title == null || Title.Length == 0 || Title.Length > 50)
                        {
                            MessageBox.Show("An error occurred : Title can't be NULL \nMust be smaller than 50", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
            }
            catch (Exception ex)
            {
                        Console.Write(ex.ToString());
                        return;
            }
            if (!checkIfPresent(ID) && !(checkIfPresent(Title)))
            {
                MessageBox.Show("Not Found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            int rowsAffected=0;
            try
            {
                        var myCon = Configuration.getInstance().getConnection();
                        string query = "DELETE FROM Project WHERE Id=@ID AND Title=@title";
                        using (SqlCommand myCommand = new SqlCommand(query, myCon))
                        {
                            myCommand.Parameters.AddWithValue("@ID", ID);
                            myCommand.Parameters.AddWithValue("@title",Title);
                            rowsAffected= myCommand.ExecuteNonQuery();
                   
                        }
                if (rowsAffected > 0)
                {
                    MessageBox.Show("Deleted");
                }
                else
                {
                    MessageBox.Show("ID and Title are of Different Projects");
                }
                
                        clr = true;
                        getfromDataBase();
                clearData();
            }
            catch (Exception ex)
            {
                        Console.WriteLine(ex.Message);
                        return;
            }                     
        }
        private void datagrvw_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try 
            { 
                int id;
                if(e.RowIndex >= 0)
                {
                    DataGridViewRow row = datagrvw.Rows[e.RowIndex];
                    id = Convert.ToInt32(row.Cells["Id"].Value);
                    idTxt.Text = id.ToString();
                    titletxt.Text = (row.Cells["Title"].Value).ToString();
                    Descriptiontxt.Text = (row.Cells["Description"].Value).ToString();
                }
            }
            catch(Exception ex)
            {
              Console.WriteLine(ex.Message);   
            }
        }
        private void bckBTN_Click(object sender, EventArgs e)
        {
            Program.FirstForm.Show();
            this.Close();
        }
    } 
}
