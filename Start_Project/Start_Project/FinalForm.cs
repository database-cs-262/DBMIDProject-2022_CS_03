﻿using Start_Project.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Start_Project
{
    public partial class FinalForm : Form
    {
        public FinalForm()
        {
            InitializeComponent();
        }

        private void FinalForm_Load(object sender, EventArgs e)
        {

        }
        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }
        private void stdntBTN_Click_1(object sender, EventArgs e)
        {
            Manage_Students newForm = new Manage_Students();
            newForm.Show();
            this.Hide();
        }
        private void prjctBTN_Click(object sender, EventArgs e)
        {
            manage_Projects newForm = new manage_Projects();
            newForm.Show();
            this.Hide();
        }
        private void GRPBTN_Click(object sender, EventArgs e)
        {
            manage_Groups newForm = new manage_Groups();
            newForm.Show();
            this.Hide();
        }
        private void grpFBTN_Click(object sender, EventArgs e)
        {
            manage_GroupStudent newForm = new manage_GroupStudent();
            newForm.Show();
            this.Hide();
        }
        private void advisorBTN_Click(object sender, EventArgs e)
        {
            manage_Advisor newForm = new manage_Advisor();
            newForm.Show();
            this.Hide();
        }
        private void evlBTN_Click(object sender, EventArgs e)
        {
            manage_Evaluations newForm = new manage_Evaluations();
            newForm.Show();
            this.Hide();
        }
        private void prjctAdvisorBTN_Click_1(object sender, EventArgs e)
        {
            ProjectAdvisor newForm = new ProjectAdvisor();
            newForm.Show();
            this.Hide();
        }
        private void mngEvalGrp_Click_1(object sender, EventArgs e)
        {
            manage_Evaluation_Group manage_Students = new manage_Evaluation_Group();
            manage_Students.Show();
            this.Hide();
        }
        private void grpPrjctBTN_Click_1(object sender, EventArgs e)
        {
            manage_Group_Project manage_Students = new manage_Group_Project();
            manage_Students.Show();
            this.Hide();
        }
        private void closeApp_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Report_Form newForm = new Report_Form();
            newForm.Show();
        }
    }
}
