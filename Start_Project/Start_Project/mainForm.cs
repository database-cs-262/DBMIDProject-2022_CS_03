﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Start_Project.Forms;
using ExcelDataReader;
using System.IO;
using System.Data.SqlClient;
using System.Collections.Generic;
using Start_Project.BL;
using Start_Project.DL;

namespace Start_Project
{
    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();
        }
   
        private void loadDatafromExcel()
        {
            string filePath = "E:\\4th Semester\\Database Lab\\Mids DATA collection.xls";
            string firstName = "", lastName = "", Contact = "", emailID = "", dep = "", session = "", Gender = "", registrationNumber = "";
            int ID = 0, gend = 0; ;
            DateTime? myDate = null;
            try
            {
                using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = ExcelReaderFactory.CreateReader(stream))
                    {
                        var dataSet = reader.AsDataSet(new ExcelDataSetConfiguration
                        {
                            ConfigureDataTable = (_) => new ExcelDataTableConfiguration
                            {
                                UseHeaderRow = true
                            }
                        });
                        var dataTable = dataSet.Tables[0];
                        foreach (DataRow row in dataTable.Rows)
                        {
                            firstName = row["First Name"].ToString();
                            lastName = row["Last Name"].ToString();
                            Contact = row["Contact"].ToString();
                            emailID = row["Email ID"].ToString();
                            registrationNumber = row["Registration Number"].ToString();
                            string[] parts = registrationNumber.Split('-');
                            try
                            {
                                ID = int.Parse(parts[parts.Length - 1]);
                                dep = parts[parts.Length - 2];
                                session = parts[0];

                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                            Gender = row["Gender"].ToString().ToLower();
                            myDate = Convert.ToDateTime(row["Date of Birth"]);

                            if (Gender[0] == 'f')
                            {
                                gend = 2;
                            }
                            else if (Gender[0] == 'm')
                            {
                                gend = 1;
                            }
                            Console.WriteLine(emailID.Length);
                            int maxLength = 30;
                            if (emailID.Length > maxLength)
                            {
                                emailID = emailID.Substring(0, maxLength - 2);
                            }
                            try
                            {
                                var myCon = Configuration.getInstance().getConnection();
                                int x = 0;
                                int insertedId=0;
                                string query = "INSERT INTO Person(FirstName,LastName,Contact,Email,DateOfBirth,Gender) Values(@firstName,@lastName,@Contact1,@emailID,@myDate,@Gender)";
                                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                                {
                                    myCommand.Parameters.AddWithValue("@firstName", firstName);
                                    myCommand.Parameters.AddWithValue("@lastName", lastName);
                                    myCommand.Parameters.AddWithValue("@Contact1", Contact);
                                    myCommand.Parameters.AddWithValue("@emailID", emailID);
                                    myCommand.Parameters.AddWithValue("@myDate", myDate);
                                    myCommand.Parameters.AddWithValue("@Gender", gend);
                                    myCommand.ExecuteNonQuery();
                                }
                                try { 
                                string fetchIdQuery = "SELECT MAX(Id) FROM Person";
                                using (SqlCommand fetchIdCommand = new SqlCommand(fetchIdQuery, myCon))
                                {
                                    object result = fetchIdCommand.ExecuteScalar();
                                    if (result != null && result != DBNull.Value)
                                    {
                                        insertedId = Convert.ToInt32(result);
                                    }
                                }
                                }
                                catch (Exception ex) { Console.WriteLine(ex.Message); }
                                try
                                {
                                    Console.WriteLine(insertedId);
                                    string query2 = "INSERT INTO Student(Id,RegistrationNo) Values(@Id,@RegNo)";
                                    using (SqlCommand myCommand2 = new SqlCommand(query2, myCon))
                                    {
                                        myCommand2.Parameters.AddWithValue("@Id", insertedId);
                                        myCommand2.Parameters.AddWithValue("@RegNo", ("" + session + "-" + dep + "-" + ID + ""));
                                        myCommand2.ExecuteNonQuery();
                                    }
                                }catch (Exception ex) { Console.WriteLine(ex.Message); }
                            }

                            catch (Exception ex) { Console.WriteLine(ex.Message); }
                            Console.WriteLine("Done");
                        }
                    }

                }
                MessageBox.Show("copy");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally
            { }
        }
        private void mainForm_Load(object sender, EventArgs e)
        {
            //loadDatafromExcel();
        }

    

        private void prjctBTN_Click_1(object sender, EventArgs e)
        {
            manage_Projects newForm = new manage_Projects();
            newForm.Show();
            this.Hide();
        }

        private void GRPBTN_Click_1(object sender, EventArgs e)
        {
            manage_Groups newForm = new manage_Groups();
            newForm.Show();
            this.Hide();
        }

        private void grpFBTN_Click_1(object sender, EventArgs e)
        {
            manage_GroupStudent newForm = new manage_GroupStudent();
            newForm.Show();
            this.Hide();
        }

        private void advisorBTN_Click_1(object sender, EventArgs e)
        {
            manage_Advisor newForm = new manage_Advisor();
            newForm.Show();
            this.Hide();
        }

        private void evlBTN_Click_1(object sender, EventArgs e)
        {
            manage_Evaluations newForm = new manage_Evaluations();
            newForm.Show();
            this.Hide();
        }

        private void stdntBTN_Click(object sender, EventArgs e)
        {
            Manage_Students manage_Students = new Manage_Students();
            manage_Students.Show();
            this.Hide();
        }

        private void mngEvalGrp_Click(object sender, EventArgs e)
        {
            manage_Evaluation_Group manage_Students = new manage_Evaluation_Group();
            manage_Students.Show();
            this.Hide();
        }
        private void GroupStudentBTN_Click(object sender, EventArgs e)
        {
            manage_GroupStudent manage_Students = new manage_GroupStudent();
            manage_Students.Show();
            this.Hide();
        }
        private void grpPrjctBTN_Click(object sender, EventArgs e)
        {
            manage_Group_Project manage_Students = new manage_Group_Project();
            manage_Students.Show();
            this.Hide();
        }
        private void prjctAdvisorBTN_Click(object sender, EventArgs e)
        {
            ProjectAdvisor newForm=new ProjectAdvisor();
            newForm.Show();
            this.Hide();
        }

        private void closeApp_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }

}
