﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Start_Project.BL
{
    internal class Person
    {   
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Contact { get; set; }
        public string EmailID { get; set; }
        public DateTime MyDate { get; set; }
        public int Gender { get; set; }
        public object ID1 { get; }
        public object FirstName1 { get; }
        public object LastName1 { get; }
        public object Contact1 { get; }
        public object EmailID1 { get; }

        public Person(int id, string firstName, string lastName, string contact, string emailID, DateTime myDate, int gender)
        {
            ID = id;
            FirstName = firstName;
            LastName = lastName;
            Contact = contact;
            EmailID = emailID;
            MyDate = myDate;
            Gender = gender;
        }

        public Person(object iD, object firstName, object lastName, object contact, object emailID)
        {
            ID1 = iD;
            FirstName1 = firstName;
            LastName1 = lastName;
            Contact1 = contact;
            EmailID1 = emailID;
        }
    }


}

