﻿namespace Start_Project
{
    partial class FinalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FinalForm));
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.grpPrjctBTN = new System.Windows.Forms.Button();
            this.stdntBTN = new System.Windows.Forms.Button();
            this.prjctBTN = new System.Windows.Forms.Button();
            this.advisorBTN = new System.Windows.Forms.Button();
            this.GRPBTN = new System.Windows.Forms.Button();
            this.grpFBTN = new System.Windows.Forms.Button();
            this.evlBTN = new System.Windows.Forms.Button();
            this.mngEvalGrp = new System.Windows.Forms.Button();
            this.prjctAdvisorBTN = new System.Windows.Forms.Button();
            this.closeApp = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.40269F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.59731F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel1, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, -2);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1007, 646);
            this.tableLayoutPanel3.TabIndex = 13;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel2.BackgroundImage")));
            this.tableLayoutPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(632, 640);
            this.tableLayoutPanel2.TabIndex = 13;
            this.tableLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel2_Paint);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.button1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(641, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 87.80488F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.19512F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(363, 640);
            this.tableLayoutPanel1.TabIndex = 14;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.20833F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.79167F));
            this.tableLayoutPanel4.Controls.Add(this.grpPrjctBTN, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.stdntBTN, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.prjctBTN, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.advisorBTN, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.GRPBTN, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.grpFBTN, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.evlBTN, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.prjctAdvisorBTN, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.mngEvalGrp, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.closeApp, 1, 4);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 5;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.47262F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.8641F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.26978F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.96748F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.9187F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(357, 555);
            this.tableLayoutPanel4.TabIndex = 13;
            // 
            // grpPrjctBTN
            // 
            this.grpPrjctBTN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpPrjctBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.grpPrjctBTN.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.grpPrjctBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpPrjctBTN.Location = new System.Drawing.Point(4, 447);
            this.grpPrjctBTN.Name = "grpPrjctBTN";
            this.grpPrjctBTN.Size = new System.Drawing.Size(171, 104);
            this.grpPrjctBTN.TabIndex = 13;
            this.grpPrjctBTN.Text = "Group Project";
            this.grpPrjctBTN.UseVisualStyleBackColor = false;
            // 
            // stdntBTN
            // 
            this.stdntBTN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.stdntBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.stdntBTN.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.stdntBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stdntBTN.Location = new System.Drawing.Point(4, 4);
            this.stdntBTN.Name = "stdntBTN";
            this.stdntBTN.Size = new System.Drawing.Size(171, 100);
            this.stdntBTN.TabIndex = 6;
            this.stdntBTN.Text = "Manage Students";
            this.stdntBTN.UseVisualStyleBackColor = false;
            // 
            // prjctBTN
            // 
            this.prjctBTN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.prjctBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.prjctBTN.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.prjctBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prjctBTN.Location = new System.Drawing.Point(182, 4);
            this.prjctBTN.Name = "prjctBTN";
            this.prjctBTN.Size = new System.Drawing.Size(171, 100);
            this.prjctBTN.TabIndex = 7;
            this.prjctBTN.Text = "Manage Projects";
            this.prjctBTN.UseVisualStyleBackColor = false;
            // 
            // advisorBTN
            // 
            this.advisorBTN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.advisorBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.advisorBTN.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.advisorBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.advisorBTN.Location = new System.Drawing.Point(4, 215);
            this.advisorBTN.Name = "advisorBTN";
            this.advisorBTN.Size = new System.Drawing.Size(171, 99);
            this.advisorBTN.TabIndex = 8;
            this.advisorBTN.Text = "Manage Advisors";
            this.advisorBTN.UseVisualStyleBackColor = false;
            // 
            // GRPBTN
            // 
            this.GRPBTN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GRPBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.GRPBTN.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.GRPBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GRPBTN.Location = new System.Drawing.Point(4, 111);
            this.GRPBTN.Name = "GRPBTN";
            this.GRPBTN.Size = new System.Drawing.Size(171, 97);
            this.GRPBTN.TabIndex = 9;
            this.GRPBTN.Text = "Manage Groups";
            this.GRPBTN.UseVisualStyleBackColor = false;
            // 
            // grpFBTN
            // 
            this.grpFBTN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpFBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.grpFBTN.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.grpFBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpFBTN.Location = new System.Drawing.Point(182, 111);
            this.grpFBTN.Name = "grpFBTN";
            this.grpFBTN.Size = new System.Drawing.Size(171, 97);
            this.grpFBTN.TabIndex = 4;
            this.grpFBTN.Text = "Group Formation";
            this.grpFBTN.UseVisualStyleBackColor = false;
            // 
            // evlBTN
            // 
            this.evlBTN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.evlBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.evlBTN.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.evlBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.evlBTN.Location = new System.Drawing.Point(182, 215);
            this.evlBTN.Name = "evlBTN";
            this.evlBTN.Size = new System.Drawing.Size(171, 99);
            this.evlBTN.TabIndex = 5;
            this.evlBTN.Text = "Manage Evaluations";
            this.evlBTN.UseVisualStyleBackColor = false;
            // 
            // mngEvalGrp
            // 
            this.mngEvalGrp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mngEvalGrp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.mngEvalGrp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.mngEvalGrp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mngEvalGrp.Location = new System.Drawing.Point(182, 321);
            this.mngEvalGrp.Name = "mngEvalGrp";
            this.mngEvalGrp.Size = new System.Drawing.Size(171, 119);
            this.mngEvalGrp.TabIndex = 10;
            this.mngEvalGrp.Text = "Group Evaluation";
            this.mngEvalGrp.UseVisualStyleBackColor = false;
            // 
            // prjctAdvisorBTN
            // 
            this.prjctAdvisorBTN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.prjctAdvisorBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.prjctAdvisorBTN.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.prjctAdvisorBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prjctAdvisorBTN.Location = new System.Drawing.Point(4, 321);
            this.prjctAdvisorBTN.Name = "prjctAdvisorBTN";
            this.prjctAdvisorBTN.Size = new System.Drawing.Size(171, 119);
            this.prjctAdvisorBTN.TabIndex = 15;
            this.prjctAdvisorBTN.Text = "Project Advisor";
            this.prjctAdvisorBTN.UseVisualStyleBackColor = false;
            // 
            // closeApp
            // 
            this.closeApp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.closeApp.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("closeApp.BackgroundImage")));
            this.closeApp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.closeApp.Location = new System.Drawing.Point(182, 447);
            this.closeApp.Name = "closeApp";
            this.closeApp.Size = new System.Drawing.Size(171, 104);
            this.closeApp.TabIndex = 16;
            this.closeApp.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(3, 564);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(357, 73);
            this.button1.TabIndex = 14;
            this.button1.Text = "Reports";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FinalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1007, 633);
            this.Controls.Add(this.tableLayoutPanel3);
            this.MaximumSize = new System.Drawing.Size(1471, 936);
            this.MinimumSize = new System.Drawing.Size(1025, 680);
            this.Name = "FinalForm";
            this.Text = "FinalForm";
            this.Load += new System.EventHandler(this.FinalForm_Load);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button grpPrjctBTN;
        private System.Windows.Forms.Button stdntBTN;
        private System.Windows.Forms.Button prjctBTN;
        private System.Windows.Forms.Button advisorBTN;
        private System.Windows.Forms.Button GRPBTN;
        private System.Windows.Forms.Button grpFBTN;
        private System.Windows.Forms.Button evlBTN;
        private System.Windows.Forms.Button prjctAdvisorBTN;
        private System.Windows.Forms.Button mngEvalGrp;
        private System.Windows.Forms.Button closeApp;
    }
}